package tests;


import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.initializePageObjects.PageFactoryInitializer;


/**
 * @Author Nikhil Bhole
 * @Date 28-09-2018
 * 
 * Test suite for document upload module: Users permitted: [Dol] 
 */

public class TestDocumentUpload extends PageFactoryInitializer {
	
	@BeforeMethod
	void login() {
		 homePage().clickonLoginLink().enterUsername("dol_admin_1").enterPassword("123456").clickLogin();
	}
	
	/**Method to upload document.*/
	void uploadDocument(String orderNumber, String machineNumber, String filePath) {
		try {
			uploadDocumentPage().enterOrderNumber(orderNumber).enterMachineNumber(machineNumber).clickBrowseFiles()
					.fileUpload(filePath);
			sleep(3000);
			uploadDocumentPage().clickUploadDocument();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * Consider "case-sensitive" file names while running this this test case.
	 * The test case fails on ubuntu as It doesnot consider texts "One" and "one" same
	 * because of the case. 
	 * Add the test case later of such requirement comes.
	 */
	
	@Test
	void checkIfMachineNumberMatchesFileName()  {
		uploadDocument("OrderIX","fiveMegaByte","/home/nikhil/Documents/TestPdf/FOO/machineone.pdf");
		uploadDocumentPage().verifyFileNameMessage();
	}
	
	
	/**
	 * Check the system behavior if upload button is clicked keeping all fields empty.
	 * All the error messages should be properly displayed.
	 */
	
	@Test
	void keepAllFieldsBlankAndClickOnUpload() {
		uploadDocumentPage().clickUploadDocument().verifyBlankLineNumberMessage().verifyBlankMachineNumberMessage().verifyBlankFileMessage();
	}
	
	/**
	 * Check if files except PDF type are not uploaded.
	 * Create test data consisting of files of different file formats.
	 * and run this test case.
	 */
	
	@Test
	void checkFileFormatValidation() {
		uploadDocument("OrderIX","cssSelector","/home/nikhil/Downloads/cssSelector.docx");
		uploadDocumentPage().verifyFileExtensionMessage();
	}
	
	/**
	 * Provide test PDF files of various sizes based on boundry value analysis.
	 * And analyze the functionality by simply running this test case.
	 */
	
	@Test
	void checkFileSizeValidation() throws Exception {
		uploadDocument("OrderIX","fiveMegaByte","/home/nikhil/Documents/TestPdf/fiveMegaByte.pdf");
		uploadDocumentPage().verifyFileSizeMessage();
	}
	
	
	/**
	 * As only DOL user can access this functionality, It is advisable to check if a 
	 * siemens user can access this functionality as well. Though this test case is covered
	 * in another test suite as well.
	 * 
	 */
	
	@Test
	void checkFileUploadFeatureAccessibility() throws Exception {
		
	}
	
	/**
	 * Test case: 
	 */
	void checkIfFileCanBeUploadedOEnterKeyPress() throws Exception {
		 uploadDocumentPage().enterMachineNumber("OrderIX").enterOrderNumber("machinethree").clickBrowseFiles().fileUpload("/home/nikhil/Documents/TestPdf/testone.pdf");
		 sleep(3000);
		 hitTab();
		 hitEnter();
		 uploadDocumentPage().verifySuccessfulDocumentUploadMessage();
	}
	
	
	@Test
	void checkIfDocumentUploadIsSuccessful() {
		uploadDocument("OrderIX","machinethree","/home/nikhil/Documents/TestPdf/FOO/machinethree.pdf");
		uploadDocumentPage().verifySuccessfulDocumentUploadMessage();
	}
	
	@Test
	void uploadFilehavingSameNameAsMachineNumberButDifferentFileFormat() {
		uploadDocument("OrderIX","testone","/home/nikhil/Documents/TestPdf/FOO/testone.txt");
		uploadDocumentPage().verifyFileExtensionMessage();
	}
	
	@Test
	void TestIfSpecialCharactersAreNotAllowedInOrderAndMachineNumber() {
		uploadDocument("%^&^&^&^&^&^","*(&*&((**(&*^&^&%^&","/home/nikhil/Documents/TestPdf/FOO/testone.txt");
		uploadDocumentPage().verifySpecialCharactersAreNotAllowedAsAOrderAndMachineNumber();
	}
	
	
	void CheckOrderNumberFormat() {
		
	}
	
	
	void CheckMachineNumberFormat() {
		
	}
	
	/**
	 * Test case: Only single file against a machine number can be uploaded.
	 * There can be multiple machine numbers against a single order number.
	 */
	
	
	void uploadMultipleFilesForOneMachineNumber() {
			
	}
	
	@Test
	void verifyBrowseFilePlaceholder() {
		uploadDocumentPage().verifyPlaceholder();
	}
	
	//https://www.google.co.in/search?ei=1UDYW9KWLcnTvwT786KYBw&q=battle+symphony+lyrics&oq=batt+lyrics&gs_l=psy-ab.1.1.0i7i30k1l10.148151.148998.0.151596.4.4.0.0.0.0.263.678.0j3j1.4.0....0...1c.1.64.psy-ab..0.4.675...0i67k1j0i7i10i30k1.0.erEN9JEJdWE
}
