package tests;


import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pageObjects.initializePageObjects.PageFactoryInitializer;

/**
 * 
 * @author Nikhil Bhole
 * Test Steps:
 * 1. Upload a test record.
 * 2. Search the uploaded record.
 * 3. Delete the record.
 */

public class TestDeleteDocumentFunctionality extends PageFactoryInitializer{
		
	@BeforeMethod
	void login() throws Exception {
		 homePage()
		.clickonLoginLink()
		.enterUsername("dol_admin_1")
		.enterPassword("123456")
		.clickLogin();
	}
	
	/*
	 * Upload a new record and delete it.
	 */
	@Test(priority=1)
	void testSetup() throws Exception {
		 	 uploadDocumentPage()
		 	.uploadDocument("betaorder","betaorderpdf", "/home/nikhil/Documents/TestPdf/betaorderpdf.pdf")
		    .verifySuccessfulDocumentUploadMessage(); // Verification point 1.
		 	 searchDocumentPage()
		 	.clickOnSearchDocumentsLink()
		 	.searchByMachineNumber("betaorderpdf")
		 	.clickSearch()
		 	.searchAndDeleteDocument("betaorderpdf");
		 	 modal().clickYes().verifyMessageAfterDeletingRecord().clickOk(); // Verification point 2.
	}
	
	@Test(priority=2)
	void clickDeleteRecordAndCancelTheOperation() {
		
	}
	
	/*
	 * Test weather record appears after deleting document. 
	 */
	@Test(priority=3)
	void testIfRecordIsDeleted() throws Exception {
			 searchDocumentPage()
		 	.clickOnSearchDocumentsLink()
		 	.searchByMachineNumber("betaorderpdf")
		 	.clickSearch()
		 	.verifyMachineNumberIsNotPresent("betaorderpdf");
	}
	
	@Test
	void verifyDeleteRecordConfirmationMessage() throws Exception {
		//Are you sure to delete this document?
		searchDocumentPage().clickOnSearchDocumentsLink().searchByMachineNumber("machineone").clickSearch().searchAndDeleteDocument("machineone");
		modal().verifyConfirmDeleteMessage();
	}
	
	/*
	 * This test case ensures that record is not deleted if user clicks "No" when he gets confirmation
	 * message on Modal Popup. 
	 */
	
	@Test
	void clickNoAfterselectingRecordToDeleteFromModal() throws Exception {
		//Are you sure to delete this document?
		searchDocumentPage().clickOnSearchDocumentsLink().searchByMachineNumber("machineone").clickSearch().searchAndDeleteDocument("machineone");
		modal().clickNo();
		searchDocumentPage().verifyMachineNumberIsPresent("machineone");
	}
}	
