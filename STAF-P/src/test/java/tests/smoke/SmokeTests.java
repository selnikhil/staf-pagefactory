package tests.smoke;

import org.testng.annotations.Test;

import pageObjects.initializePageObjects.PageFactoryInitializer;

/**
 * 
 * @author Nikhil Bhole.
 *
 */

public class SmokeTests extends PageFactoryInitializer{
	
	@Test(priority=0)
	void verifyTheApplcationLaunchesSuccessfully() {
		homePage().verifyHomePageIsDisplayed();
	}
	
	@Test(priority=0)
	void verifyTheLogoIsDisplayedProperly() {
		homePage().verifyHomePageIsDisplayed();
	}
	
	/* Test case id: 
	 * Test case checks that a DOL user should be able to log into the system and then redirected
	 * to Dol landing page [upload section].
	 * Test Data comprises of some valid and invalid users[dol] credentials. Test case should pass for valid 
	 * data. Also later it checks the landing page. Invalid user details should be restricted.
	 */
	
	
	
	@Test(priority=1)
	void verifyLoginFunctionalityForDolUser() {
		
	}
	
	/* Test case id: 
	 * Test case checks that a siemens user should be able to log into the system and then redirected
	 * to siemens landing page [upload section].
	 * Test Data comprises of some valid and invalid users[siemens] credentials. Test case should pass for valid 
	 * data. Also later it checks the landing page. Invalid user details should be restricted.
	 */
	
	@Test(priority=2)
	void verifyLoginFunctionalityForSiemensUser() {
		
	}
	
	
}	
