package tests.smoke;


import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pageObjects.initializePageObjects.PageFactoryInitializer;

public class SanityTests extends PageFactoryInitializer{
	//login application. as a DOL
	
	//navigate to all links 
	//check if all links are working fine.
	//logout afterwards.
	
	@BeforeMethod
	void testLogin() {
		homePage().loginToApplication("dol_admin_1","123456").verifySuccessfulLogin("dol_admin_1");
	}
	
	void verifyHomePageIsDisplayed() {
		navigationBar().gotoHomePage();
	}
	
	
	void verifySearchDocumentPageIsDisplayed() {
		navigationBar().gotoSearchDocumentsPage();
		searchDocumentPage().verifyUserIsOnSearchDocumentPage();
	}
	
	
	void verifyUploadDocumentPageIsDisplayed() {
		navigationBar().gotoUploadDocumentsPage();
		uploadDocumentPage().verifyUserIsOnUploadDocumentPage();
	}
	
	void verifyClickingDisplaysAllOptionsCorrectly() {
		
	}
	
	void verifyIfChangePasswordPageIsDisplayed() {
		
	}
	
	@Test(priority=2)
	void verifyLogoutFunctionality() {
		navigationBar().clickOnUsername().logoutApplication();
	}
}

/**
 * 
 */
  
