package tests;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pageObjects.initializePageObjects.PageFactoryInitializer;

public class TestSingleFileDownloadFunctionality extends PageFactoryInitializer{
	
	
	/*
	 * create new directory in downloads to keep temporary downlaoded files.
	 */
	static void createDirectory(String folderName) {
		new File("/home/nikhil/Downloads/" + folderName).mkdir();
	}
	
	/*
	 * Removes downloaded data after successful test case completion.
	 */
	
	static void deleteDirectoryWithFiles(String folderName) {
		try {
			FileUtils.deleteDirectory(new File("/home/nikhil/Downloads/" + folderName));
		} catch (IOException e) {
			
		}
	}
	
	String getFileName() {
		String fileName = "";
		File file = new File("/home/nikhil/Downloads/temp_downloads");
		File[] files = file.listFiles();
		if (files.length == 1) {
			System.out.println(files[0].getName());
			return files[0].getName();
		}
		if (files.length > 1) {
			for (int i = 0; i < files.length; i++) {
				fileName = files[i].getName() + " ";
			}
		}
		return "xtwo";
	}
	
	List getFilesList() {
		List<String> fileNames = new ArrayList<>();
		File file = new File("/home/nikhil/Downloads/temp_downloads");
		File[] files = file.listFiles();
		for (int i = 0; i < files.length; i++) {
			fileNames.add(files[i].getName());
		}
		return fileNames;
	}
	
	int getFileCount(String directoryPath) {
		return  new File(directoryPath).list().length;
	}
	/*
	 * Taking machine name as an argument here as requirement says that machine number and file name should be same.
	 */
	@BeforeMethod
	void login() throws Exception {
		homePage().clickonLoginLink().enterUsername("dol_admin_1").enterPassword("123456").clickLogin();
		searchDocumentPage().clickOnSearchDocumentsLink();

		searchDocumentPage().searchByOrderNumber("orderX").clickSearch().setRecordsPerPage("10");
	}
	
	@Test
	void downloadSingleFile() {
		createDirectory("temp_downloads");
		searchDocumentPage().downloadFile("xtwelve");
		System.out.println("File is downloaded. Cheking the file presense in the directory");
		sleep(5000);
		Assert.assertTrue(getFileName().equals("xtwelve.pdf"));
		System.out.println(getFileName());
		deleteDirectoryWithFiles("temp_downloads");
	}
	
		@Test
		void clickPaginationNextAndDownloadFile() {
			createDirectory("temp_downloads");
			searchDocumentPage().clickPaginationNext().searchDocumentPage().downloadFile("xtwentythree");
			System.out.println("File is downloaded. Cheking the file presense in the directory");
			sleep(5000);
			Assert.assertTrue(getFileName().equals("xtwentythree.pdf"));
			System.out.println(getFileName());
			deleteDirectoryWithFiles("temp_downloads");
		}
	
	/*
	 * click on pagination last. Click any file present and 
	 */
	
	@Test
	void goToLastPageAndDownloadFile() {
		createDirectory("temp_downloads");
		searchDocumentPage().clickPaginationLast().sleep(5000);
		searchDocumentPage().downloadFile("xtwentyfour");
		System.out.println("File is downloaded. Cheking the file presense in the directory");
		sleep(5000);
		Assert.assertTrue(getFileName().equals("xtwentyfour.pdf"));
		System.out.println(getFileName());
		deleteDirectoryWithFiles("temp_downloads");
	}
	
	@Test
	void downloadMultipleFilesFromSamePage() {
		createDirectory("temp_downloads");
		searchDocumentPage().downloadMultipleFiles();
		deleteDirectoryWithFiles("temp_downloads");
	}
	
	@Test
	void downloadFilesFromDifferentPages(){
		createDirectory("temp_downloads");
		searchDocumentPage().searchDocumentPage().downloadFile("xtwo").clickPaginationNext()
		.downloadFile("xthirteen")
		.clickPaginationNext().downloadFile("xtwentyfour");
		
		sleep(5000);
		
		System.out.println("Files: " + getFilesList());
		
		deleteDirectoryWithFiles("temp_downloads");
	}
	
	
}
