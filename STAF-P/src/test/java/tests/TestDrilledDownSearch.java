package tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pageObjects.initializePageObjects.PageFactoryInitializer;

/**
 * 
 * @author Nikhil
 * More enhanced search feature that will filter out already searched results and 
 * displays more specific results.
 */
public class TestDrilledDownSearch extends PageFactoryInitializer {
	
	@BeforeMethod
	void setUpTest() throws Exception {
		routine().signInAsDolUser();
	}
	
	@Test
	void foo() {
		
	}
}
