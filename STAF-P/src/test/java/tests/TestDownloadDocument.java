package tests;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.apache.ant.compress.taskdefs.Unzip;
import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.initializePageObjects.PageFactoryInitializer;

/**
 * 
 * @author Nikhil Bhole.
 *
 */

public class TestDownloadDocument extends PageFactoryInitializer{
	
	static final String DOWNLOAD_PATH = "/home/nikhil/Downloads/";
	
	public static void deleteFilesForPathByPrefix(final Path path, final String prefix) {
	    try (DirectoryStream<Path> newDirectoryStream = Files.newDirectoryStream(path, prefix + "*")) {
	        for (final Path newDirectoryStreamItem : newDirectoryStream) {
	            Files.delete(newDirectoryStreamItem);
	        }
	    } catch (final Exception e) { // empty
	    }
	}
	
	public static File lastFileModified(String dir) {
	    File fl = new File(dir);
	    File[] files = fl.listFiles(new FileFilter() {          
	        public boolean accept(File file) {
	            return file.isFile();
	        }
	    });
	    long lastMod = Long.MIN_VALUE;
	    File choice = null;
	    for (File file : files) {
	        if (file.lastModified() > lastMod) {
	            choice = file;
	            lastMod = file.lastModified();
	        }
	    }
	    return choice;
	}
	
	/*
	 * create new directory in downloads to keep temporary downlaoded files.
	 */
	static void createDirectory(String folderName) {
		new File(DOWNLOAD_PATH + folderName).mkdir();
	}
	
	/*
	 * Removes downloaded data after successful test case completion.
	 */
	
	static void deleteDirectoryWithFiles(String folderName) {
		try {
			FileUtils.deleteDirectory(new File(DOWNLOAD_PATH + folderName));
		} catch (IOException e) {
			
		}
	}
	
	String getFileName() {
		String fileName = "";
		File file = new File(DOWNLOAD_PATH + "temp_downloads");
        File[] files = file.listFiles();
        System.out.println(files.length);
        if(files.length==1) {
        	System.out.println(files[0].getName());
        	return files[0].getName();
        }
        if(files.length>1) {
        	for(int i=0;i<files.length;i++) {
        		fileName = files[i].getName() + " ";
        	}
        }
        return "xtwo";
	}
	
	List<String> getFilesList() {
		List<String> fileNames = new ArrayList<>();
		File file = new File(DOWNLOAD_PATH + "temp_downloads/Dol_Documents");
        File[] files = file.listFiles();
        for(int i=0;i<files.length;i++) {
        	fileNames.add(files[i].getName());
        }
        return fileNames;
	}
	
	int getFileCount(String directoryPath) {
		return  new File(directoryPath).list().length;
	}
	
	void unZip(String destinationFolder) {
		 Unzip unzipper = new Unzip();
		 unzipper.setSrc(new File(DOWNLOAD_PATH +  "temp_downloads/" + destinationFolder + ".zip"));
		 unzipper.setDest(new File(DOWNLOAD_PATH + "temp_downloads/" + destinationFolder ));
		 unzipper.execute();
	}
	
	@BeforeMethod
	void login() throws Exception {
		 createDirectory("temp_downloads");  
		 homePage()
		.clickonLoginLink()
		.enterUsername("dol_admin_1")
		.enterPassword("123456")
		.clickLogin();
		 searchDocumentPage()
	    .clickOnSearchDocumentsLink();
	}
	
	
	void verifyValidationMessageIfRecordIsDownloadedWithoutSelecting() throws Exception {
		  searchDocumentPage().searchByOrderNumber("orderxi").clickSearch().setRecordsPerPage("10").downloadFile().verifyFileDownloadErrorMessage();
	}
	
	
	void verifyValidationMessageWhenClickedDownloadAllAfterSelctingSelectAll() throws Exception {
		  searchDocumentPage().searchByOrderNumber("").clickSearch().setRecordsPerPage("10").downloadFile().verifyFileDownloadErrorMessage();
	}
	
	
	void verifyDownloadedFilesInDirectory() throws Exception {
		 createDirectory("temp_downloads");  
		 searchDocumentPage().searchByOrderNumber("orderxi").clickSearch().setRecordsPerPage("All").sleep(1000);
		 searchDocumentPage().checkAllCheckboxes().downloadFile();
	     sleep(10000);
	     System.out.println("Checking file system.");
	     Unzip unzipper = new Unzip();
		 unzipper.setSrc(lastFileModified("/home/nikhil/Downloads/temp_downloads"));
		 String fileName = lastFileModified("/home/nikhil/Downloads/temp_downloads").getName();
		 int pos = fileName.lastIndexOf(".");
		 if (pos > 0) {
			 fileName = fileName.substring(0, pos);
		 }
		 
		 unzipper.setDest(new File(DOWNLOAD_PATH + "temp_downloads/" + fileName));
		 unzipper.execute();
	     
		 int fileCount = new File("/home/nikhil/Downloads/temp_downloads/Dol_Documents").list().length;
		 
		 Assert.assertEquals(fileCount, 10 , "File count doesnot match.");
		 deleteDirectoryWithFiles("temp_downloads");
	}
	
	
	void downlodFilesFromDifferentPagesInDirectory() throws Exception {
		 searchDocumentPage()
		.searchByOrderNumber("orderxi")
		.clickSearch()
		.setRecordsPerPage("All")
		.checkAllCheckboxes()
		.downloadFile();
	     sleep(10000);
	     System.out.println("Checking file system.");
	     
	     Unzip unzipper = new Unzip();
		 unzipper.setSrc(lastFileModified("/home/nikhil/Downloads"));
		 String fileName = lastFileModified("/home/nikhil/Downloads").getName();
		 int pos = fileName.lastIndexOf(".");
		 if (pos > 0) {
			 fileName = fileName.substring(0, pos);
		 }
		 unzipper.setDest(new File(DOWNLOAD_PATH + fileName ));
		 unzipper.execute();
	     int fileCount = new File(DOWNLOAD_PATH + fileName).list().length;
		 Assert.assertEquals(fileCount, 54 , "File count doesnot match.");
		 deleteDirectoryWithFiles(DOWNLOAD_PATH + "temp_downloads");
		
	}
	
	
	void verifyDownloadingMultipleFilesFromDifferentPages() throws Exception{
		 String[] expectedFiles = {"machineten", "machinefifteen", "machinefiftytwo"};
		 createDirectory("temp_downloads"); 
		 searchDocumentPage()
		.searchByOrderNumber("orderxi")
		.clickSearch().selectFileToDownload("machineten")
		.clickPaginationNext().selectFileToDownload("machinefifteen")
		.clickPaginationLast().selectFileToDownload("machinefiftytwo").downloadFile();
		 sleep(5000);
		 unZip("Dol_Documents");
		 System.out.println(getFilesList());
		 Assert.assertEquals(getFilesList().toArray(), expectedFiles);
		 deleteDirectoryWithFiles(DOWNLOAD_PATH + "temp_downloads");
	}
	
	/*
	 * This test case downloads multiple files from single page. Also this makes sure is the checkboxes
	 * are un-checked after downloading files.
	 */
	
	
	void verifyDownloadingMultipleFilesFromSinglePage() throws Exception {
		 String[] expectedFiles = {"xtwo", "xthree", "xfour"};
		 createDirectory("temp_downloads"); 
		 searchDocumentPage().searchByOrderNumber("orderx");
		 
		 for(String ef : expectedFiles) {
			 searchDocumentPage().clickSearch().selectFileToDownload(ef).downloadFile();
			 sleep(1000);
		 }
		 sleep(5000);
		 unZip("Dol_Documents");
		 System.out.println(getFilesList());
		 Assert.assertEquals(getFilesList().toArray(), expectedFiles);
		 deleteDirectoryWithFiles(DOWNLOAD_PATH + "temp_downloads");
	}
	
	@Test
	void verifySelectAll() throws Exception {
		 searchDocumentPage().searchByOrderNumber("orderxi").clickSearch().sleep(1000);
		 searchDocumentPage().selectAllRecords().sleep(2000);
		 searchDocumentPage().downloadFile();
	     sleep(1000);
	     System.out.println("Checking file system.");
	     Unzip unzipper = new Unzip();
		 unzipper.setSrc(lastFileModified("/home/nikhil/Downloads/temp_downloads"));
		 String fileName = lastFileModified("/home/nikhil/Downloads/temp_downloads").getName();
		 int pos = fileName.lastIndexOf(".");
		 if (pos > 0) {
			 fileName = fileName.substring(0, pos);
		 }
		 
		 unzipper.setDest(new File(DOWNLOAD_PATH + "temp_downloads/" + fileName));
		 unzipper.execute();
	     
		 int fileCount = new File("/home/nikhil/Downloads/temp_downloads/Dol_Documents").list().length;
		 
		 Assert.assertEquals(fileCount, 55 , "File count doesnot match.");
		 
	}
	
	@AfterTest
	void deleteDownloadedData() {
		deleteDirectoryWithFiles("temp_downloads");
	}
}
