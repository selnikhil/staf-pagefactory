package tests;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import pageObjects.initializePageObjects.PageFactoryInitializer;

public class TestLoginFunctionality extends PageFactoryInitializer {
	
	@Test(priority=2,dataProvider = "Authentication")
	public void verifyLoginForDifferentDataSets(String username, String password) throws Exception {
		 homePage()
		.clickonLoginLink()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin();

		if (getUrlTitle().contains("")) {
			test.log(LogStatus.PASS, "Test case passed.");
		} else {
			test.log(LogStatus.FAIL, "Page title doesn't match.");
		}
	}
	
	@Test(priority=1)
	public void invalidLoginTest_EmptyUserEmptyPassword() throws Exception {
		homePage().clickonLoginLink().clickLogin();
		boolean b = getUrlTitle().contains("WWW.DOLGROUP2.DOMSIT.COM");
		 if(b==true) {
			 test.log(LogStatus.PASS, "Test case failed.");
		 }else {
			 test.log(LogStatus.FAIL, "Page title doesn't match.");
		 }
	}
	
	 @DataProvider(name = "Authentication")
	 
	  public static Object[][] credentials() {
	 
	        return new Object[][] { { "dol_admin_1", "Test@123" }, { "siemens_admin_1", "Test@123" },
	        						{ "dol_admin_1", "123456" },   {"siemens_admin_1", "123456" }
	        						};
	 
	  }
}
