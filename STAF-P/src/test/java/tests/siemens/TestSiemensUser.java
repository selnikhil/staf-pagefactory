package tests.siemens;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pageObjects.initializePageObjects.PageFactoryInitializer;

public class TestSiemensUser extends PageFactoryInitializer{
	
	@BeforeMethod
	void login() {
		 homePage()
		.clickonLoginLink()
		.enterUsername("siemens_admin_1")
		.enterPassword("123456")
		.clickLogin();
	}
	
	/* 
	 * This test case checks user associated with siemens login has 
	 * landed on right page. 
	 */
	
	@Test(description="")
	void checkLandingPageIsSearchDocumentPage() throws Exception {
		searchDocumentPage()
	   .verifyPage();
	}
	
	/* 
	 * This test case checks user is un-authorized to upload docs
	 * on the system.
	 */
	
	@Test(description="Test if the user is not authorized to upload documents on the portal")
	void checkIfThereIsNoDocumentUploadFunctionalityPresent() {
		 searchDocumentPage()
		.verifyLinkPresent("Upload Documents");
	}
	
	@Test
	void checkFunctionalityForEmptySearch() {
		  searchDocumentPage().clickSearch().verifyEmptySearchMessage();
	}
	
	@Test
	void checkForEditDocumentFunctionality() {
		
	}
	
	@Test
	void checkForDeleteDocumentFunctionality() {
		
	}
	
}
