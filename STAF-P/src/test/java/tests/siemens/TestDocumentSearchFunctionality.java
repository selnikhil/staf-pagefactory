package tests.siemens;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.initializePageObjects.PageFactoryInitializer;

/**
 * 
 * @author Nikhil Bhole
 * You go, giving up your home, you are not alone.
 */

public class TestDocumentSearchFunctionality extends PageFactoryInitializer {
	
	@BeforeMethod
	void login() throws Exception {
		 homePage().clickonLoginLink().enterUsername("siemens_admin_1").enterPassword("123456").clickLogin();
		 searchDocumentPage().clickOnSearchDocumentsLink();
	}
	

	@Test
	void testSearchByEnteringOrderNumberOnly() {
		 searchDocumentPage().searchByOrderNumber("orderX").clickSearch().verifyOrderNumberFromGrid("orderX");
	}
	
	@Test
	void downloadSigleFileFromFirstPage() {
		searchDocumentPage().downloadFile("xfour");
	}
	
	@Test
	void downloadSingleFileFromFirstPage() {
		 searchDocumentPage().searchByOrderNumber("orderX").clickSearch().verifyOrderNumberFromGrid("orderX");
	}
}
