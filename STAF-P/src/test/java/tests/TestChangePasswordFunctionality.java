package tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import pageObjects.initializePageObjects.PageFactoryInitializer;

/**
 * 
 * @author Nikhil Bhole QA.
 * 
 * NOTE: Before running this test suite, run "change password validation" test suite to
 * ensure the system is satisfying preconditions for this functionality. Which will
 * ensure maximum test case coverage.
 *
 */

public class TestChangePasswordFunctionality extends PageFactoryInitializer{
	
	public static final String USERNAME = "test_user_1";
	public static final String CURRENT_PASSWORD = "123456";
	public static final String NEW_PASSWORD = "654321";

	
	/**
	 * Change password of a logged in user and verify if the password change message
	 * is displayed properly.
	 */
	@Test(priority=1)
	void changePassword() {
		homePage().loginToApplication(USERNAME, CURRENT_PASSWORD);
		navigationBar().clickOnUsername().gotoChangePasswordPage();
		changePasswordPage().changePassword(CURRENT_PASSWORD, NEW_PASSWORD, NEW_PASSWORD);
		sleep(1000);
		Assert.assertTrue(modal().getMessageFromModal().contains("Password updated successfully."));
		modal().clickOk();
		navigationBar().clickOnUsername().logoutApplication();
	}
	
	
	/**
	 *  As the password is changed now, this test case ensures that the user cannot 
	 *  log in using old password.
	 */
	@Test(priority=2)
	void loginUsingOldPassword() {
		homePage().loginToApplication(USERNAME, CURRENT_PASSWORD);
		homePage().verifyInvalidLoginErrorMessage();
	}
	
	/**
	 *	As the password is changed now, this test case ensures that the user can 
	 *  log in using new password.
	 */
	@Test(priority=3)
	void loginUsingNewPassword() {
		homePage().loginToApplication(USERNAME, NEW_PASSWORD);
		navigationBar().verifyLoggedInUser(USERNAME);
	}
	
	/**
	 * Reset the changed password so that the test suite can run 
	 * multiple times.
	 */
	@Test(priority=4)
	void resetPassword() {
		homePage().loginToApplication(USERNAME, NEW_PASSWORD);
		navigationBar().clickOnUsername().gotoChangePasswordPage();
		changePasswordPage().changePassword(NEW_PASSWORD, CURRENT_PASSWORD, CURRENT_PASSWORD);
		sleep(1000);
	}
	/**
	 * Login using initial password.
	 */
	@Test(priority=5)
	void loginUsingResetPassword() {
		homePage().loginToApplication(USERNAME, CURRENT_PASSWORD);
		navigationBar().verifyLoggedInUser(USERNAME);
	}
}
