package tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pageObjects.initializePageObjects.PageFactoryInitializer;

public class TestPagination extends PageFactoryInitializer{
	int TOTAL_RECORDS;
	
	@BeforeMethod
	void login() throws Exception {
		 homePage()
		.clickonLoginLink()
		.enterUsername("dol_admin_1")
		.enterPassword("123456")
		.clickLogin();
		
	}
	
	void test() throws Exception {
		 searchDocumentPage()
		.searchByOrderNumber("orderxi")
		.clickSearch()
		.setRecordsPerPage("10")
		.verifyNumberOfRecordsInDatatable(10)
		.setRecordsPerPage("25")
		.verifyNumberOfRecordsInDatatable(25)
		.setRecordsPerPage("50")
		.verifyNumberOfRecordsInDatatable(50);
	}
	
	
	void verifyPaginationMessage() {
		searchDocumentPage().verifyPaginationMessageDisplaysCorrectNumberOfRecords();
	}
	
	@Test(priority=1)
	void testPaginationForAllRecords() throws Exception {
		 	 searchDocumentPage()
			.clickOnSearchDocumentsLink()
			.searchByOrderNumber("orderxi")
			.clickSearch()
			.setRecordsPerPage("All")
			.verifyNumberOfRecordsInDatatable(searchDocumentPage().getSerialNumbers().size());
		 	TOTAL_RECORDS = searchDocumentPage().getSerialNumbers().size();
		 	System.out.println(TOTAL_RECORDS);
	}

	@Test(priority=2,dependsOnMethods="testPaginationForAllRecords")
	void testIfPaginationExsists() throws Exception {
		 searchDocumentPage()
			.clickOnSearchDocumentsLink()
			.searchByOrderNumber("orderX")
			.clickSearch()
			.setRecordsPerPage("10");
			
		searchDocumentPage().verifyIfPaginationExsists().paginationSolution(TOTAL_RECORDS,10);
	}
	
	/**
	 * 23-10
	 */
}
