package tests;



import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.testng.annotations.Test;

import pageObjects.initializePageObjects.PageFactoryInitializer;

public class TestOpenPDFFunctionality extends PageFactoryInitializer{
	 
	/*
	  *  OrderXII -> For automated test purpose. Associate different files with this order number.
	 	 Open each file one by one and check the content along with the correct file is opened
	 	 by clicking link.
	 */
	
	
	@Test
	void login() throws Exception {
	
		homePage().clickonLoginLink().enterUsername("dol_admin_1").enterPassword("123456").clickLogin();
		searchDocumentPage().clickOnSearchDocumentsLink().searchByOrderNumber("orderx").clickSearch().
		setRecordsPerPage("10");
		searchDocumentPage().openPdf("xone");
		
		switchAndClose();
		   
	
	
		
	}
	
	void getAllPdfs() {
		System.out.println(searchDocumentPage().getFileNames());

	}
	
	void switchAndClose() throws IOException {
		ArrayList<String> tabs2 = new ArrayList<String>(getWebDriver().getWindowHandles());
		getWebDriver().switchTo().window(tabs2.get(1));
		URL url = new URL(getWebDriver().getCurrentUrl());
		System.out.println(url.toString());

		InputStream is = url.openStream();
		BufferedInputStream fileToParse = new BufferedInputStream(is);
		PDDocument document = null;
		try {
			document = PDDocument.load(fileToParse);
			int noOfPages= document.getNumberOfPages();
			System.out.println(noOfPages);
			String output = new PDFTextStripper().getText(document);
			System.out.println(output);
		} finally {
			if (document != null) {
				document.close();
			}
			fileToParse.close();
			is.close();
		  }
	}
		
}
