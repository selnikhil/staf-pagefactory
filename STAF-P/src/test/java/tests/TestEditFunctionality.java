package tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pageObjects.initializePageObjects.PageFactoryInitializer;

public class TestEditFunctionality extends PageFactoryInitializer{
	
	final String FOLDER_PATH = "/home/nikhil/Documents/AutomationDatasets/PDF/";
	final String FILE_NAME = "ECR33105537"; // same as machine number.
	final String ORDER_NUMBER = "ECRTESTORDER01";
	
	/**
	 * This method runs before every test case. 
	 * This will login user and navigate to search document page.
	 */
	@BeforeMethod
	void testSetUp() throws Exception {
		 homePage().clickonLoginLink().enterUsername("dol_admin_1").enterPassword("123456").clickLogin();
		 searchDocumentPage().clickOnSearchDocumentsLink();
	}
	
	void openDocumentDetailsInEditMode() {
		searchDocumentPage().searchByMachineNumber(FILE_NAME).clickSearch().searchAndEditDocument(FILE_NAME);
	}
	/**
	 * File name and machine number should be same.
	 * So changing file name should display an error. 
	 */
	@Test
	void verifyUpdateButtonText() {
		openDocumentDetailsInEditMode();
		uploadDocumentPage().verifUpdateButtonTextOnEditForm();
	}
	
	@Test
	void verifyFormHeader() {
		openDocumentDetailsInEditMode();
		uploadDocumentPage().verifyEditFormHeaderText();
	}
	

	void clickCloseButtonFromHeader() {
		
	}

	@Test
	void changeFileName() {
		/**
		 * Change file name[machine number] and click on update.
		 * Error message should be displayed. 
		 */
		openDocumentDetailsInEditMode();
		uploadDocumentPage().clearMachineNumber().enterMachineNumber("ECR33105538").clickUploadDocument().sleep(2000);
		uploadDocumentPage().verifyFileNameMessageOnEdit().closeModal();
		modal().clickNo();
	}
	
	@Test
	void changeFile() throws Exception {
		/**
		 * Upload different file and click on upload.
		 * Error message should be displayed as machine number is going to vary. 
		 */
		openDocumentDetailsInEditMode();
		uploadDocumentPage().ClickBrowseFilesInEdit().uploadFile(FOLDER_PATH + "ECR33105538").sleep(2000);
		uploadDocumentPage().clickUploadDocument().verifyFileNameMessageOnEdit().closeModal();
		modal().clickYes();
		uploadDocumentPage().verifyFileNameMessageOnEdit().closeModal();
		modal().clickNo();
		sleep(2000);
	}
	
	@Test
	void clearAllFieldsAndClickUpdate() {
		openDocumentDetailsInEditMode();
		uploadDocumentPage().clearOrderNumber().clearMachineNumber().clickUploadDocument().sleep(2000);
		uploadDocumentPage().verifyBlankLineNumberMessage(). verifyBlankMachineNumberMessage();
	}
	
	@Test
	void clearOrderNumberAndClickUpdate() {
		openDocumentDetailsInEditMode();
		uploadDocumentPage().clearOrderNumber().clickUploadDocument().sleep(2000);
		uploadDocumentPage().verifyBlankLineNumberMessage();
	}
	
	@Test
	void uploadFileWithUnsupportedFileFormat() throws Exception {
		openDocumentDetailsInEditMode();
		uploadDocumentPage().ClickBrowseFilesInEdit().uploadFile(FOLDER_PATH + "ECR33105537.txt").sleep(2000);
		uploadDocumentPage().clickUploadDocument().verifyFileExtensionMessageOnEdit().closeModal();
		modal().clickYes();
		uploadDocumentPage().verifyFileExtensionMessageOnEdit().closeModal();
		modal().clickNo();
		sleep(2000);
	}
}
