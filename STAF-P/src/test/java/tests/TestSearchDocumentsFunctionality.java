package tests;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pageObjects.initializePageObjects.PageFactoryInitializer;

public class TestSearchDocumentsFunctionality extends PageFactoryInitializer{

	@BeforeMethod
	void login() throws Exception {
		 homePage().clickonLoginLink().enterUsername("dol_admin_1").enterPassword("123456").clickLogin();
		 searchDocumentPage().clickOnSearchDocumentsLink();
	}
	
	@Test
	void test() throws Exception {
		 searchDocumentPage()
		.searchByOrderNumber("orderxi")
		.clickSearch()
		.setRecordsPerPage("10")
		.selectAllRecords();
	}
	
	
	/*
	 * to test if the search functionality is returning valid results.
	 * test data: Please enter valid order number for which there are records in database.
	 */
	
	@Test
	void testSearchByEnteringOrderNumberOnly() {
		 searchDocumentPage()
		.searchByOrderNumber("orderxi")
		.clickSearch()
		.setRecordsPerPage("All")
		.selectAllRecords()
		.verifyOrderNumberFromGrid("orderxi");
	}
	
	/*
	 * to test if the search functionality is returning valid results.
	 * test data: Please enter valid order number for which there are records in database.
	 */
	
	@Test
	void testSearchByEnteringMachineNumberOnly() {
		 searchDocumentPage()
		.searchByMachineNumber("machinethree")
		.clickSearch()
		.setRecordsPerPage("All")
		.isMachineNumberPresent("machinethree");
		 System.out.println("Total records: " + searchDocumentPage().getNumberOfRecords());
		 Assert.assertEquals(searchDocumentPage().getNumberOfRecords(), 1,"Gird is displaying more than 1 record. Might be a problem."); //
	}
	
	@Test
	void testSearchByMachineNumberAndOrderNumber() {
		 searchDocumentPage()
		.searchByOrderNumber("orderxi")
		.searchByMachineNumber("machinethree")
		.clickSearch()
		.setRecordsPerPage("All")
		.isMachineNumberPresent("machinethree");
		 System.out.println("Total records: " + searchDocumentPage().getNumberOfRecords());
		 Assert.assertEquals(searchDocumentPage().getNumberOfRecords(), 1,"Gird is displaying more than 1 record. Might be a problem."); //
	}
	
	@Test
	void loaderIsDisplayedIfInBetweenSearch() {
		 	 searchDocumentPage()
			.searchByOrderNumber("orderxi")
			.clickSearch().isLoaderDisplayed();
	}
	
	@Test
	void verifyIfProperMessageIsDisplayedOnEmptySearchOperations() {
		//S.O./Line Number or Machine Number is required.
		searchDocumentPage().clickSearch().verifyEmptySearchMessage();
	}
	
}
