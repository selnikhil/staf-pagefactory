package tests;


import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pageObjects.initializePageObjects.PageFactoryInitializer;


public class TestEditDocument extends PageFactoryInitializer{
	
	final String FILE_PATH = "/home/nikhil/Documents/TestPdf/betaorderpdf.pdf";
	final String ORIGINAL_FILE_NAME = "";
	final String CHANGED_FILE_NAME = "";
	
	
	@BeforeMethod
	void login() throws Exception {
		 homePage()
		.clickonLoginLink()
		.enterUsername("dol_admin_1")
		.enterPassword("123456")
		.clickLogin();
		 
		 uploadDocumentPage()
		.uploadDocument("betaorder","betaorderpdf", FILE_PATH)
		.verifySuccessfulDocumentUploadMessage(); // Verification point 1.
		
	}
	
	void openRecordInEditMode() throws Exception {
		 searchDocumentPage()
	 	.clickOnSearchDocumentsLink()
	 	.searchByMachineNumber("betaorderpdf")
	 	.clickSearch()
	 	.searchAndEditDocument("betaorderpdf");
	}
	
	/*
	 * Upload a new record and delete it.
	 */
	@Test
	void changeMachineNumber() throws Exception {
		 openRecordInEditMode();
		 uploadDocumentPage().clearMachineNumber().enterMachineNumber("changedMachineNumber").clickUploadDocument().sleep(2000);
		 uploadDocumentPage().verifyFileNameMessageOnEdit().closeModal();
		 modal().clickYes().sleep(2000);
		 uploadDocumentPage()
		.clearMachineNumber()
		.enterMachineNumber("changedMachineNumber");
		 uploadDocumentPage().verifyFileNameMessageOnEdit().closeModal();
		 modal().clickNo();
	}
	
	@Test
	void changeFile() throws Exception {
		 openRecordInEditMode();
		 uploadDocumentPage().ClickBrowseFilesInEdit()
		.uploadFile(FILE_PATH)
		.clickUploadDocument()
		.sleep(2000);
		 uploadDocumentPage().verifyFileNameMessageOnEdit().closeModal();
		 modal().clickYes();
		 uploadDocumentPage().verifyFileNameMessageOnEdit().closeModal();
		 modal().clickNo();
		 sleep(2000);
	}
	
	@Test
	void changeOrderNumberWithoutChangingOtherDetails() throws Exception {
		 openRecordInEditMode();
		 uploadDocumentPage().clearOrderNumber();
		 sleep(2000);
		 uploadDocumentPage().enterOrderNumber("betaordernew").clickUploadDocument();
		 modal().clickOk();
		 sleep(2000);
		 //Search the updated record. If found, pass the test case.
		 searchDocumentPage().clickOnSearchDocumentsLink().searchByOrderNumber("betaordernew").clickSearch().verifyOrderNumberFromGrid("betaordernew");
		 
		// searchDocumentPage().clickOnSearchDocumentsLink().searchByOrderNumber("betaorder").clickSearch();
		// Assert.assertFalse(searchDocumentPage().isOrderNumberPresent("betaorder"));
	}
	
	
	void testIfOldRecordIsNotPresentAfterEdit() throws Exception {
		 
	}
}
