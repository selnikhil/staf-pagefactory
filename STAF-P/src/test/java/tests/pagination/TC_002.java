package tests.pagination;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pageObjects.initializePageObjects.PageFactoryInitializer;

public class TC_002 extends PageFactoryInitializer {
	/**
	 * Check if previous link is disabled on Page 1.
	 */
	
	@BeforeMethod
	void login() throws Exception {
		 homePage().signInAsDolUser();
		 searchDocumentPage().clickOnSearchDocumentsLink();
	}
	
	@Test
	void test() throws Exception {
		 searchDocumentPage()
		.searchByOrderNumber("orderxi")
		.clickSearch();
		
	}
	
}
