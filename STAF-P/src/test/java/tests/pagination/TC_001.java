package tests.pagination;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pageObjects.initializePageObjects.PageFactoryInitializer;


public class TC_001 extends PageFactoryInitializer {
	
	@BeforeMethod
	void login() throws Exception {
		 homePage().signInAsDolUser();
		 searchDocumentPage().clickOnSearchDocumentsLink();
	}
	
	@Test
	void test() throws Exception {
		 searchDocumentPage()
		.searchByOrderNumber("orderxi")
		.clickSearch()
		.setRecordsPerPage("10")
		.verifyNumberOfRecordsInDatatable(10)
		.setRecordsPerPage("25")
		.verifyNumberOfRecordsInDatatable(25)
		.setRecordsPerPage("50")
		.verifyNumberOfRecordsInDatatable(50)
		.setRecordsPerPage("all")
		.verifyNumberOfRecordsInDatatable(searchDocumentPage().getSerialNumbers().size());;
	}
	
}
