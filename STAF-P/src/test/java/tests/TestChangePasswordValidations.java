package tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.initializePageObjects.PageFactoryInitializer;

public class TestChangePasswordValidations extends PageFactoryInitializer {
	
	@BeforeMethod
	void setupTest() {
		homePage().loginToApplication("test_user_1", "123456");
		navigationBar().clickOnUsername().gotoChangePasswordPage();
	}
	
	@Test
	void clickChangePasswordWithAllBlankFields() {
		changePasswordPage().changePassword("", "", "").verifyEmptyMessageOnEmptyOldPassword().verifyEmptyMessageOnEmptyNewPassword().verifyEmptyMessageOnEmptyConfirmPassword();
	}
	
	@Test
	void enterAllFieldsExceptOldPasswordAndClickOnChangePassword() {
		 changePasswordPage().changePassword("", "testtest", "testtest").verifyEmptyMessageOnEmptyOldPassword();
	}
	
	@Test
	void enterAllFieldsExceptNewPasswordAndClickOnChangePassword() {
		changePasswordPage().changePassword("testtest", "", "testtest").verifyEmptyMessageOnEmptyNewPassword();
	}
	
	@Test
	void enterAllFieldsExceptConfirmPasswordAndClickOnChangePassword() {
		changePasswordPage().changePassword("testtest", "testtest", "").verifyEmptyMessageOnEmptyConfirmPassword();
	}
	
	@Test
	void clickChangePasswordByKeepingConfirmPasswordBlank() {
		 changePasswordPage().changePassword("", "", "testtest").verifyEmptyMessageOnEmptyOldPassword().verifyEmptyMessageOnEmptyNewPassword();
	}
	
	@Test
	void clickChangePasswordByKeepingNewPasswordBlank() {
		changePasswordPage().changePassword("testtest", "", "testtest").verifyEmptyMessageOnEmptyNewPassword();
	}
	
	@Test
	void clickChangePasswordByKeepingOldPasswordBlank() {
		changePasswordPage().changePassword("", "testtest", "testtest").verifyEmptyMessageOnEmptyOldPassword();
	}
	
	@Test
	void newPasswordAndConfirmPasswordShouldBeMatchedKeppingOldPasswordBlank() {
		changePasswordPage().changePassword("", "testtest", "testtest1").verifyEmptyMessageOnEmptyOldPassword().verifyNewPasswordAndConfirmPasswordMatches();
	}
	
	@Test
	void newPasswordAndConfirmPasswordShouldBeMatched() {
		changePasswordPage().changePassword("", "testtest", "testtest1").verifyNewPasswordAndConfirmPasswordMatches();
	}
	
	@Test
	void verifyNewPasswordLength() {
		changePasswordPage().changePassword("", "123", "123").verifyEmptyMessageOnEmptyConfirmPassword();
	}
	
	@Test
	void enterValidNewAndConfirmPasswordButInvalidCurrentPassword() {
		changePasswordPage().changePassword("12345678", "123456", "123456").verifyIncorrectCurrentPassword();
	}
	
	@Test
	void enterValidNewAndConfirmPasswordButBlankCurrentPassword() {
		changePasswordPage().changePassword("", "123456", "123456").verifyEmptyMessageOnEmptyOldPassword().verifyIncorrectCurrentPassword();
	}
	
	@Test
	void verifyChangePasswordOnEnteringAllValidDetails() {
		
	}
}
