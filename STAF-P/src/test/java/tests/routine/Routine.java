package tests.routine;

import pageObjects.initializePageObjects.PageFactoryInitializer;

public class Routine extends PageFactoryInitializer {
	
	public void signInAsSiemensUser() {
		homePage().clickonLoginLink().enterUsername("siemens_admin_1").enterPassword("123456").clickLogin();
	}
	
	public void signInAsDolUser() {
		homePage().clickonLoginLink().enterUsername("dol_admin_1").enterPassword("123456").clickLogin();
	}
}
