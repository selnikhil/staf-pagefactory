package tests.selectall;


import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.initializePageObjects.PageFactoryInitializer;


/**
 * 
 * @author Nikhil Bhole
 * @date 
 */
public class TC_002 extends PageFactoryInitializer {
/**
 * Click select all check box. Un check the check box and click on Download.
 * Error message should be displayed.
 */
	
	@BeforeMethod
	void login() throws Exception {
		 TS_001.createDirectory("temp_downloads");  
		 homePage().signInAsDolUser();
		 searchDocumentPage().clickOnSearchDocumentsLink();
	}
	
	@Test
	void clickSelectAllAndDeselectSomeRecordsAndDownload() throws Exception {
		 searchDocumentPage().searchByOrderNumber("orderxi").clickSearch().sleep(1000);
		 searchDocumentPage().selectAllRecords().sleep(1000);
		 searchDocumentPage().selectAllRecords().sleep(1000);
		 searchDocumentPage().downloadFile().verifyFileDownloadErrorMessage();
	}
	
	
	
}
