package tests.selectall;


import java.io.File;

import org.apache.ant.compress.taskdefs.Unzip;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pageObjects.initializePageObjects.PageFactoryInitializer;

public class TC_010 extends PageFactoryInitializer {

/**
 * select a record on page 1
 * click next
 * select 1 records from second page
 * click next
 * select all records from page 3.
 * click previous and select any record
 * goto last page and select one record.
 * click download.
 * [total number of downloaded files should be 14]
 */
	

	public static final String DOCUMENT_PATH = "/home/nikhil/Downloads/temp_downloads/Dol_Documents";
	public static final String DOWNLOAD_LOCATION = "/home/nikhil/Downloads/temp_downloads";
	
	
	@BeforeMethod
	void login() throws Exception {
		 TS_001.createDirectory("temp_downloads");  
		 homePage().signInAsDolUser();
		 searchDocumentPage().clickOnSearchDocumentsLink();
	}
	
	@Test
	void tc_010() throws Exception {
		  searchDocumentPage().searchByOrderNumber("orderxi").clickSearch().sleep(1000);
		  searchDocumentPage().sleep(2000);
		  searchDocumentPage().selectFileToDownload("machinenine")
		 .clickPaginationNext()
		 .selectFileToDownload("machineeighteen")
		 .clickPaginationNext()
		 .checkAllRecordsFromGridMaually().sleep(2000);
		  searchDocumentPage().clickPaginationPrevious()
		 .selectFileToDownload("machineeleven")
		 .clickPaginationLast()
		 .selectFileToDownload("machinefifftyfive").downloadFile();
		  sleep(1000);
	      System.out.println("Checking file system.");
	      
	      System.out.println("Checking file system.");
		     Unzip unzipper = new Unzip();
			 unzipper.setSrc(TS_001.lastFileModified(DOWNLOAD_LOCATION));
			 String fileName = TS_001.lastFileModified(DOWNLOAD_LOCATION).getName();
			 int pos = fileName.lastIndexOf(".");
			 if (pos > 0) {
				 fileName = fileName.substring(0, pos);
			 }
			 unzipper.setDest(new File(TS_001.DOWNLOAD_PATH + "temp_downloads/" + fileName));
			 unzipper.execute();
		     int fileCount = new File(DOCUMENT_PATH).list().length;
	}
	
	@AfterTest
	void deleteDownloadedData() {
		TS_001.deleteDirectoryWithFiles("temp_downloads");
	}
}
