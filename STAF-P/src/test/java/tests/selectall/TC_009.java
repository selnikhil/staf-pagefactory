package tests.selectall;

import java.io.File;

import org.apache.ant.compress.taskdefs.Unzip;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pageObjects.initializePageObjects.PageFactoryInitializer;

public class TC_009 extends PageFactoryInitializer {
	/**
	
	 * search record and download random files from first page.
	 */
	
	public static final String DOCUMENT_PATH = "/home/nikhil/Downloads/temp_downloads/Dol_Documents";
	public static final String DOWNLOAD_LOCATION = "/home/nikhil/Downloads/temp_downloads";
	
	
	@BeforeMethod
	void login() throws Exception {
		 TS_001.createDirectory("temp_downloads");  
		 homePage().signInAsDolUser();
		 searchDocumentPage().clickOnSearchDocumentsLink();
	}
	
	@Test
	void tc_009() throws Exception {
		 searchDocumentPage().searchByOrderNumber("orderxi").clickSearch().sleep(1000);
		 searchDocumentPage().sleep(2000);
		 searchDocumentPage().selectFileToDownload("machineten").selectFileToDownload("machinesix").selectFileToDownload("machineone").sleep(1500);
		 searchDocumentPage().downloadFile();
	     sleep(1000);
	     System.out.println("Checking file system.");
	     Unzip unzipper = new Unzip();
		 unzipper.setSrc(TS_001.lastFileModified(DOWNLOAD_LOCATION));
		 String fileName = TS_001.lastFileModified(DOWNLOAD_LOCATION).getName();
		 int pos = fileName.lastIndexOf(".");
		 if (pos > 0) {
			 fileName = fileName.substring(0, pos);
		 }
		 unzipper.setDest(new File(TS_001.DOWNLOAD_PATH + "temp_downloads/" + fileName));
		 unzipper.execute();
	     
		 int fileCount = new File(DOCUMENT_PATH).list().length;
		 Assert.assertEquals(fileCount,3 , "File count doesnot match.");
		 Assert.assertFalse(TS_001.getFilesList().contains("machineten"));
		 Assert.assertFalse(TS_001.getFilesList().contains("machinesix"));
		 Assert.assertFalse(TS_001.getFilesList().contains("machineone"));
	}
	
	@AfterTest
	void deleteDownloadedData() {
		TS_001.deleteDirectoryWithFiles("temp_downloads");
	}
}
