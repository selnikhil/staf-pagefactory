package tests.selectall;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.apache.ant.compress.taskdefs.Unzip;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pageObjects.initializePageObjects.PageFactoryInitializer;

public class TC_007 extends PageFactoryInitializer {
/**
 * goto last page
 * select all records on that page
 * click download 
 * verifyFiles
 * 
 */
	
	@BeforeMethod
	void login() throws Exception {
		 TS_001.createDirectory("temp_downloads");  
		 homePage().signInAsDolUser();
		 searchDocumentPage().clickOnSearchDocumentsLink();
	}
	
	@Test
	void tc_007() throws Exception {
		 searchDocumentPage().searchByOrderNumber("orderxi").clickSearch().sleep(1000);
		 searchDocumentPage().clickPaginationLast().checkAllRecordsFromGridMaually();
		 sleep(2000);
		 List<String> files = searchDocumentPage().getFileNames();
		 searchDocumentPage().downloadFile();
		 
		 sleep(1000);
	     System.out.println("Checking file system.");
	     Unzip unzipper = new Unzip();
		 unzipper.setSrc(TS_001.lastFileModified("/home/nikhil/Downloads/temp_downloads"));
		 String fileName = TS_001.lastFileModified("/home/nikhil/Downloads/temp_downloads").getName();
		 int pos = fileName.lastIndexOf(".");
		 if (pos > 0) {
			 fileName = fileName.substring(0, pos);
		 }
		 unzipper.setDest(new File(TS_001.DOWNLOAD_PATH + "temp_downloads/" + fileName));
		 unzipper.execute();
	     
		 int fileCount = new File("/home/nikhil/Downloads/temp_downloads/Dol_Documents").list().length;
		 List<String> fileNames = Arrays.asList(new File("/home/nikhil/Downloads/temp_downloads/Dol_Documents").list());
				 
		 System.out.println(TS_001.getFilesList());
		 Assert.assertEquals(fileCount, 5 , "File count doesnot match."); // verifyIfSelectedRecordsInGridAndFileSystemAreEqual
		 searchDocumentPage().verifyIfSelectedRecordsInGridAndFileSystemAreEqual(files,fileNames);
		 
	}
	
	@AfterTest
	void deleteDownloadedData() {
		TS_001.deleteDirectoryWithFiles("temp_downloads");
	}
}
