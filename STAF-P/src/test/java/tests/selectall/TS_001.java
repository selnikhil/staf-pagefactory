package tests.selectall;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.apache.ant.compress.taskdefs.Unzip;
import org.apache.commons.io.FileUtils;

public class TS_001 {

	public static final String DOWNLOAD_PATH = "/home/nikhil/Downloads/";
	
	public static void deleteFilesForPathByPrefix(final Path path, final String prefix) {
	    try (DirectoryStream<Path> newDirectoryStream = Files.newDirectoryStream(path, prefix + "*")) {
	        for (final Path newDirectoryStreamItem : newDirectoryStream) {
	            Files.delete(newDirectoryStreamItem);
	        }
	    } catch (final Exception e) { // empty
	    }
	}
	
	public static File lastFileModified(String dir) {
	    File fl = new File(dir);
	    File[] files = fl.listFiles(new FileFilter() {          
	        public boolean accept(File file) {
	            return file.isFile();
	        }
	    });
	    long lastMod = Long.MIN_VALUE;
	    File choice = null;
	    for (File file : files) {
	        if (file.lastModified() > lastMod) {
	            choice = file;
	            lastMod = file.lastModified();
	        }
	    }
	    return choice;
	}
	
	/*
	 * create new directory in downloads to keep temporary downlaoded files.
	 */
	static void createDirectory(String folderName) {
		new File(DOWNLOAD_PATH + folderName).mkdir();
	}
	
	/*
	 * Removes downloaded data after successful test case completion.
	 */
	
	static void deleteDirectoryWithFiles(String folderName) {
		try {
			FileUtils.deleteDirectory(new File(DOWNLOAD_PATH + folderName));
		} catch (IOException e) {
			
		}
	}
	
	String getFileName() {
		String fileName = "";
		File file = new File(DOWNLOAD_PATH + "temp_downloads");
        File[] files = file.listFiles();
        System.out.println(files.length);
        if(files.length==1) {
        	System.out.println(files[0].getName());
        	return files[0].getName();
        }
        if(files.length>1) {
        	for(int i=0;i<files.length;i++) {
        		fileName = files[i].getName() + " ";
        	}
        }
        return "xtwo";
	}
	
	public static List<String> getFilesList() {
		List<String> fileNames = new ArrayList<>();
		File file = new File(DOWNLOAD_PATH + "temp_downloads/Dol_Documents");
        File[] files = file.listFiles();
        for(int i=0;i<files.length;i++) {
        	fileNames.add(files[i].getName());
        }
        return fileNames;
	}
	
	int getFileCount(String directoryPath) {
		return  new File(directoryPath).list().length;
	}
	
	void unZip(String destinationFolder) {
		 Unzip unzipper = new Unzip();
		 unzipper.setSrc(new File(DOWNLOAD_PATH +  "temp_downloads/" + destinationFolder + ".zip"));
		 unzipper.setDest(new File(DOWNLOAD_PATH + "temp_downloads/" + destinationFolder ));
		 unzipper.execute();
	}
}
