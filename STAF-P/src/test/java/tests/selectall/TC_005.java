package tests.selectall;

import java.io.File;

import org.apache.ant.compress.taskdefs.Unzip;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pageObjects.initializePageObjects.PageFactoryInitializer;
// NOT COMPLETED. DO NOT RUN
public class TC_005 extends PageFactoryInitializer {
/**
 * 1. Select all.
 * 2. set pagination to 10.
 * 3. deselect 10 records from first page.
 * 4. click download.
 */
	
	@BeforeMethod
	void login() throws Exception {
		 TS_001.createDirectory("temp_downloads");  
		 homePage().signInAsDolUser();
		 searchDocumentPage().clickOnSearchDocumentsLink();
	}
	
	@Test
	void tc_005() throws Exception {
		searchDocumentPage().searchByOrderNumber("orderxi").clickSearch().sleep(1000);
		searchDocumentPage().selectAll(); sleep(1000);
		searchDocumentPage().selectSequentilRecordsFromPage(10).sleep(1000);
		
		  System.out.println("Checking file system.");
		     Unzip unzipper = new Unzip();
			 unzipper.setSrc(TS_001.lastFileModified("/home/nikhil/Downloads/temp_downloads"));
			 String fileName = TS_001.lastFileModified("/home/nikhil/Downloads/temp_downloads").getName();
			 int pos = fileName.lastIndexOf(".");
			 if (pos > 0) {
				 fileName = fileName.substring(0, pos);
			 }
			 unzipper.setDest(new File(TS_001.DOWNLOAD_PATH + "temp_downloads/" + fileName));
			 unzipper.execute();
		     
			 int fileCount = new File("/home/nikhil/Downloads/temp_downloads/Dol_Documents").list().length;
			 
			 System.out.println(TS_001.getFilesList());
			 
			 Assert.assertEquals(fileCount, 45 , "File count doesnot match.");
		
	}
	
	@AfterTest
	void deleteDownloadedData() {
		TS_001.deleteDirectoryWithFiles("temp_downloads");
	}
	
}
