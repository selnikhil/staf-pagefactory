package tests.selectall;

import org.testng.annotations.BeforeMethod;

import pageObjects.initializePageObjects.PageFactoryInitializer;

public class TC_008 extends PageFactoryInitializer {
/**
 *  1. goto last page
 *  2. click select all
 *  3. go in any middle page
 *  4. click select all
 */
	
	@BeforeMethod
	void login() throws Exception {
		 TS_001.createDirectory("temp_downloads");  
		 homePage().signInAsDolUser();
		 searchDocumentPage().clickOnSearchDocumentsLink();
	}
	
}
