package tests.selectall;

public class TC_011 {
	//Pagination check.
	
	int totalRecords;
	int recordsPerPage;
	
	static int totalPages(int totalRecords, int recordsPerPage) {
		if(recordsPerPage == 10) {
			return totalRecords/recordsPerPage;
		}else if(recordsPerPage == 25){
			return totalRecords/recordsPerPage;
		}else if(recordsPerPage == 50){
			return totalRecords/recordsPerPage;
		}else if(recordsPerPage == 100){
			return totalRecords/recordsPerPage;
		}
		return 0;
	}
	
	public static void main(String args[]) {
		System.out.println(totalPages(60,10));
	}
}
