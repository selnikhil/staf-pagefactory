/**
 * 
 */
package pageObjects.initializePageObjects;

import org.openqa.selenium.support.PageFactory;

import controllers.BaseMethod;
import pageObjects.pages.ChangePasswordPageObjects;
import pageObjects.pages.DocumentUploadPageObjects;
import pageObjects.pages.GoogleHomePageObjects;
import pageObjects.pages.HomePageObjects;
import pageObjects.pages.NavigationPageObjects;
import pageObjects.pages.PopupModal;
import pageObjects.pages.SearchDocumentPageObjects;
import tests.routine.Routine;


/**
 * @author Nikhil Bhole
 * @date July 04, 2014
 * 
 */
public class PageFactoryInitializer extends BaseMethod {
	
	public GoogleHomePageObjects googleHomePage() {
		return PageFactory.initElements(getWebDriver(), GoogleHomePageObjects.class);
	}
	
	public HomePageObjects homePage() {
		return PageFactory.initElements(getWebDriver(), HomePageObjects.class);
	}
	
	public DocumentUploadPageObjects uploadDocumentPage() {
		return PageFactory.initElements(getWebDriver(), DocumentUploadPageObjects.class);
	}
	
	public SearchDocumentPageObjects searchDocumentPage() {
		return PageFactory.initElements(getWebDriver(), SearchDocumentPageObjects.class);
	}
	
	public PopupModal modal() {
		return PageFactory.initElements(getWebDriver(), PopupModal.class);
	}
	
	public NavigationPageObjects navigationBar() {
		return PageFactory.initElements(getWebDriver(), NavigationPageObjects.class);
	}
	
	public ChangePasswordPageObjects changePasswordPage() {
		return PageFactory.initElements(getWebDriver(), ChangePasswordPageObjects.class);
	}
	
	public Routine routine() {
		return PageFactory.initElements(getWebDriver(), Routine.class);
	}
	
	
}
