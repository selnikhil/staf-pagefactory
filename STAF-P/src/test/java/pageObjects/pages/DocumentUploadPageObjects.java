package pageObjects.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import pageObjects.initializePageObjects.PageFactoryInitializer;
import utils.FluentWaiting;

/**
 * @Author Nikhil Bhole
 * @Date 27-Sept-2018
 */

public class DocumentUploadPageObjects extends PageFactoryInitializer{
	
	/* WEB ELEMENTS */
	
	@FindBy(id="OrderNumber")
	private WebElement txtOrderOrLineNumber;
	
	@FindBy(id="MotorNumber")
	private WebElement txtMachineNumber;
	
	@FindBy(id="DemographicBrowse")
	private WebElement btnBrowseFiles;
	
	@FindBy(id="UploadButton")
	private WebElement btnUploadFile;
	
	@FindBy(xpath="//*[@id=\"ErrorMessageDocumentFile\"]")  //*[@id="ErrorMessageUpdateDocumentFile"]
	private WebElement msgFileDetails;
	
	@FindBy(xpath="//*[@id=\"ErrorMessageUpdateDocumentFile\"]")  
	private WebElement msgFileDetailsInEdit;
	
	@FindBy(xpath="//*[@id=\"UploadSONumber\"]")
	private WebElement msgLineNumber; 
	
	@FindBy(xpath="//*[@id=\"UploadMachineNumber\"]")
	private WebElement machineNumber;
	
	@FindBy(xpath="//*[@id=\"ErrorMessageUpdateDocumentFile\"]")
	private WebElement msgMachineNumberOnEdit;
	
	@FindBy(xpath="//*[@id=\"successmessage\"]")
	private WebElement msgSuccessfulDocumentUpload;
	
	@FindBy(id="CloseModal")
	private WebElement btnCloseEditModal;
	
	@FindBy(xpath="//*[@id=\"body\"]/fieldset[3]/div/span/input[2]")
	private WebElement btnBrowseFilesEditModal;
	
	@FindBy(xpath="//*[@id=\"image_file\"]")
	private WebElement placeholderBrowseFiles;
	
	@FindBy(id = "ErrorMessageSONumber")
	private WebElement errorBlankSoNumberOnEdit;
	
	/* APPLICATION MODULES */
	
	
	/* Insert Data */
	public DocumentUploadPageObjects enterOrderNumber(String orderNumber) {
		txtOrderOrLineNumber.sendKeys(orderNumber);
		return this;
	}
	
	public DocumentUploadPageObjects enterMachineNumber(String orderNumber) {
		txtMachineNumber.sendKeys(orderNumber);
		return this;
	}
	
	public DocumentUploadPageObjects uploadFile(String filePath) throws Exception {
		fileUpload(filePath);
		return this;
	}
	
	public DocumentUploadPageObjects clickBrowseFiles() {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, btnBrowseFiles);
		click(btnBrowseFiles);	
		return this;
	}
	
	public DocumentUploadPageObjects clickUploadDocument() {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, btnUploadFile);
		click(btnUploadFile);	
		return this;
	}
	
	public DocumentUploadPageObjects verifyPageTitle() throws Exception {
		FluentWaiting.waitForTitleToBe(5, 500, "Google");
		return this;
	}
	
	public DocumentUploadPageObjects verifyFileSizeMessage() {
		Assert.assertEquals(msgFileDetails.getText(), "Please upload pdf file size less than 1MB.");
		return this;
	}
	
	public DocumentUploadPageObjects verifyFileExtensionMessage() {
		Assert.assertEquals(msgFileDetails.getText(), "Please upload PDF file only.");
		return this;
	}
	
	public DocumentUploadPageObjects verifyFileExtensionMessageOnEdit() {
		Assert.assertEquals(msgFileDetails.getText(), "Please upload PDF file only.");
		return this;
	}
	
	public DocumentUploadPageObjects verifyFileNameMessage() {
		Assert.assertEquals(msgFileDetails.getText(), "Machine number and file name should be same.");
		return this;
	}
	
	public DocumentUploadPageObjects verifyFileNameMessageOnEdit() {
		Assert.assertEquals(msgMachineNumberOnEdit.getText(), "Machine number and file name should be same.");
		return this;
	}
	
	public DocumentUploadPageObjects verifyBlankLineNumberMessage() {
		Assert.assertEquals(msgLineNumber.getText(), "S.O. Number / Line Number required.");
		return this;
	}
	
	public DocumentUploadPageObjects verifyBlankMachineNumberMessage() {
		Assert.assertEquals(machineNumber.getText(), "Machine Number required.");
		return this;
	}
	
	public DocumentUploadPageObjects verifyBlankFileMessage() {
		Assert.assertEquals(msgFileDetails.getText(), "Document file required.","Something's missing. CTSO");
		return this;
	}
	
	public DocumentUploadPageObjects verifySuccessfulDocumentUploadMessage() {
		FluentWaiting.waitUntillElementToBeVisible(5, 500, msgSuccessfulDocumentUpload);
		Assert.assertEquals(msgSuccessfulDocumentUpload.getText(), "Document Uploaded Successfully.");
		return this;
	}
	
	public DocumentUploadPageObjects uploadDocument(String orderNumber, String machineNumber, String filePath) {
		try {
			uploadDocumentPage().enterOrderNumber(orderNumber).enterMachineNumber(machineNumber).clickBrowseFiles()
					.fileUpload(filePath);
			sleep(3000);
			uploadDocumentPage().clickUploadDocument();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}
	
	public DocumentUploadPageObjects clearMachineNumber() {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, txtMachineNumber);
		txtMachineNumber.clear();
		return this;
	}
	
	public DocumentUploadPageObjects clearOrderNumber() {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, txtOrderOrLineNumber);
		txtOrderOrLineNumber.clear();
		return this;
	}
	
	public DocumentUploadPageObjects closeModal() {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, btnCloseEditModal);
		btnCloseEditModal.click();
		return this;
	}
	
	public DocumentUploadPageObjects ClickBrowseFilesInEdit() {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, btnBrowseFilesEditModal);
		btnBrowseFilesEditModal.click();
		return this;
	}
	
	public DocumentUploadPageObjects verifyUserIsOnUploadDocumentPage() {
		Assert.assertTrue(btnUploadFile.isDisplayed()); 
		return this;
	}
	
	public DocumentUploadPageObjects verifUpdateButtonTextOnEditForm() {
		Assert.assertTrue(btnUploadFile.getText().contains("Update"), "Botton text is not matched."); 
		return this;
	}
	
	public DocumentUploadPageObjects verifyEditFormHeaderText() {
		Assert.assertTrue(modal().getModalTitle().contains("Edit Record"), "Header text is not matched."); 
		return this;
	}
	
	//Please enter alphabets and numbers only.
	public DocumentUploadPageObjects verifySpecialCharactersAreNotAllowedAsAOrderAndMachineNumber() {
		Assert.assertTrue(msgLineNumber.getText().contains("Please enter alphabets and numbers only."));
		Assert.assertTrue(machineNumber.getText().contains("Please enter alphabets and numbers only."));
		return this;
	}
	
	public DocumentUploadPageObjects verifyPlaceholder() {
		Assert.assertTrue(placeholderBrowseFiles.getAttribute("placeholder").contains("File type: PDF and Max size: 1 MB"));
		return this;
	}
}
