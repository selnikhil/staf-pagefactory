package pageObjects.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import pageObjects.initializePageObjects.PageFactoryInitializer;
import utils.FluentWaiting;

/**
 * @Author Nikhil Bhole
 * @Date 04-July-2018
 */

public class HomePageObjects extends PageFactoryInitializer {

	@FindBy(xpath="//*[@id='loginButton']/span")
	private WebElement loginLink;
	
	@FindBy(id="name")
	private WebElement txtUsername;
	
	@FindBy(id="password")
	private WebElement txtPassword;
	
	@FindBy(id="login")
	private WebElement btnLogin;
	
	@FindBy(id="InvalidUserServerError")
	private WebElement loginError;
	
	// 
	public HomePageObjects clickonLoginLink() {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, loginLink);
		click(loginLink);	
		return this;
	}
	
	public HomePageObjects enterUsername(String username) {
		txtUsername.sendKeys(username);
		return this;
	}
	
	public HomePageObjects enterPassword(String password) {
		txtPassword.sendKeys(password);
		return this;
	}
	
	public HomePageObjects clickLogin() {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, btnLogin);
		click(btnLogin);	
		return this;
	}
	
	public HomePageObjects loginToApplication(String username, String password) {
		clickonLoginLink().enterUsername(username).enterPassword(password).clickLogin();
		return this;
	}
	
	/**
	 * 
	 * @param user
	 * @return
	 */
	public HomePageObjects verifySuccessfulLogin(String userName){
		if(userName.equalsIgnoreCase("dol_admin_1")) {
			uploadDocumentPage().verifyUserIsOnUploadDocumentPage();
		}
		else {
			searchDocumentPage().verifyUserIsOnSearchDocumentPage();
		}
		return this;
	}
	
	public HomePageObjects verifyHomePageIsDisplayed() {
		Assert.assertTrue(loginLink.isDisplayed());
		return this;
	}
	
	public HomePageObjects verifyInvalidLoginErrorMessage() {
		Assert.assertTrue(loginError.getText().contains("Invalid username or password."));
		return this;
	}
	
	public HomePageObjects signInAsDolUser() {
		clickonLoginLink().enterUsername("dol_admin_1").enterPassword("123456").clickLogin();
		return this;
	}
	
}
