package pageObjects.pages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import pageObjects.initializePageObjects.PageFactoryInitializer;
import utils.FluentWaiting;

/**
 * 
 * @author Nikhil Bhole
 * Search Document Page objects.
 */

public class SearchDocumentPageObjects extends PageFactoryInitializer {
	
	@FindBy(xpath="//a[text()='Search Documents']")
	private WebElement searchDocumentLink;
	
	@FindBy(id="SearchOrderNumber")
	private WebElement txtOrderNumber;
	
	@FindBy(id="SearchMotorNumber")
	private WebElement txtMachineNumber;
	
	@FindBy(id="SearchDocument")
	private WebElement btnSearch;
	
	@FindBy(xpath="//*[@id=\"SearchDocumentTable_length\"]/label/select")
	private WebElement selectbxRecordsPerPage;
	
	@FindBy(id="DownloadSelectedbtn")
	private WebElement btnDownloadRecord;
	
	@FindBy(id="select_all")
	private WebElement chkbxSelectAll;
	
	@FindBy(id="SearchDocumentTBody")
	private WebElement tableElement;
	
	@FindAll({@FindBy(xpath = ".//tr")})
	public List<WebElement> rowElements;
	
	@FindAll({@FindBy(xpath = ".//td")})
	public List<WebElement> cellElements;
	
	
	@FindBy(xpath ="//*[@id=\"SearchDocErrorMessage\"]")
	private WebElement msgEmptySearch;
	
	@FindBy(xpath = "//a[text()='xone.pdf']")
	private WebElement testPDFLink;
	
	//xpath for table column machine number FOR FINIDING ALL ELEMENTS.
	@FindAll({@FindBy(xpath = "//*[@id=\"SearchDocumentTBody\"]/tr/td[4]")})
	public List<WebElement>  tdMachineNumber;
	
	//xpath for table column order number FOR FINIDING ALL ELEMENTS.
	@FindAll({@FindBy(xpath = "//*[@id=\"SearchDocumentTBody\"]/tr[1]/td[3]")})
	public List<WebElement>  tdOrderNumber;
	
	//xpath for table column serial number FOR FINIDING ALL ELEMENTS.
	@FindAll({@FindBy(xpath = "//*[@id=\"SearchDocumentTBody\"]/tr/td[1]")})
	public List<WebElement>  tdSerialNumber;
	
	@FindAll({@FindBy(xpath = "//*[@id=\"SearchDocumentTBody\"]/tr[1]/td[4]")})
	public List<WebElement>  dynamicDeleteButton;
	
	@FindAll({@FindBy(xpath = "//*[@id=\"SearchDocumentTBody\"]/tr/td[0]")})
	public List<WebElement>  allrecords;
	
	@FindBy(xpath = "//*[@id=\"ChecboxError\"]")
	private WebElement msgFileDownload;
	
	
	//xpath for number-of-records. SearchDocumentTable_info
	@FindBy(id = "SearchDocumentTable_info")
	private WebElement paginationDetails;
	
	@FindAll({@FindBy(xpath = "//*[@id=\"SearchDocumentTable_paginate\"]//a")})
	public List<WebElement>  pagination;
	
	@FindAll({@FindBy(className = "paginate_button")})
	public List<WebElement> pages;
	
	@FindBy(partialLinkText = "Next")
	private WebElement paginationNext;
	
	@FindBy(partialLinkText = "Last")
	private WebElement paginationLast;
	
	@FindBy(partialLinkText = "Previous")
	private WebElement paginationPrevious;
	
	@FindBy(partialLinkText = "First")
	private WebElement paginationFirst;
	
	@FindAll({@FindBy(xpath = "//input[type='checkbox']")})
	private List<WebElement> checkBox;
	
	String[] paginationEntries = {"First","Previous","Next","Last"};
	
	//*[@id="CB107"]
	@FindBy(xpath = ("//input[@type='checkbox'])[position()=1]"))
	private WebElement checkBox1;
	
	@FindBy(xpath = "//*[@id=\"CB108\"]")
	private WebElement checkBox2;
	
	/** * * * App Modules */
	
	@FindBy(id = "spinnerUpload")
	private WebElement spinnerLoader;
	
	@FindAll({@FindBy(xpath = "//a[contains(text(), '.pdf')]/@href")})
	public List<WebElement>  pdfLinks;
	
	@FindBy(xpath = "//*[@id=\"SearchDocumentTable_filter\"]/input")
	private WebElement tableSearch;
	
	/*
	 * App Modules.
	 */
	
	
	public SearchDocumentPageObjects clickOnSearchDocumentsLink() throws Exception {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, searchDocumentLink);
		click(searchDocumentLink);	
		return this;		
	}

	
	public SearchDocumentPageObjects searchByOrderNumber(String orderNumber) {
		txtOrderNumber.sendKeys(orderNumber);
		return this;
	}
	
	public SearchDocumentPageObjects searchByMachineNumber(String machineNumber) {
		txtMachineNumber.sendKeys(machineNumber);
		return this;
	}
	
	public SearchDocumentPageObjects searchByBoth(String orderNumber, String machineNumber) {
		txtMachineNumber.sendKeys(orderNumber, machineNumber);
		return this;
	}
	
	public SearchDocumentPageObjects clickSearch() {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, btnSearch);
		click(btnSearch);
		return this;
	}
	
	public SearchDocumentPageObjects setRecordsPerPage(String records) {
		selectByVisibleText(selectbxRecordsPerPage, records);
		return this;
	}
	
	public SearchDocumentPageObjects selectAllRecords() {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, chkbxSelectAll);
		click(chkbxSelectAll);	
		return this;
	}
	
	public List <String> iterateTable(String xpath){
		 List<String> tableData = new ArrayList<String>();
		 List<WebElement> column1 = tdMachineNumber;
		 int row_num=1;
		  for (WebElement tdElement : column1 ){
			  tableData.add(tdElement.getText());
			  row_num++;
		  }
	   return tableData;
	}
	
	public String getPosition(List test, String category) {
		return String.valueOf(test.indexOf(category)+1);
	}
	
	public SearchDocumentPageObjects verifyPageTitle() throws Exception 
	{
		FluentWaiting.waitForTitleToBe(5, 500, "Google");
		return this;
	}
	
	public SearchDocumentPageObjects verifyPage() throws Exception {
		Assert.assertTrue(btnSearch.getAttribute("value").equals("Search"),"Bruh!! You are landed on wrong page pal. Test Case is failed..");
		return this;
	}
	
	public SearchDocumentPageObjects verifyLinkPresent(String value){
	  getWebDriver().getPageSource().contains(value);
	  return this;
	}
	
	/**
	 * @param machine number or Line/Order number.
	 */
	public SearchDocumentPageObjects searchAndDeleteDocument(String document) {
		List<String> records = new ArrayList<String>();
		int row_num=1;
		  for (WebElement tdElement : tdMachineNumber ){
			  records.add(tdElement.getText());
			  row_num++;
		  }
		  String temp = String.valueOf(records.indexOf(document)+1);
		  getWebDriver().findElement(By.xpath(".//*[@id='SearchDocumentTBody']/tr["+temp+"]/td[6]/.//*[@id=\"deletebtn\"]")).click();
		return this;
	}
	
	/**
	 * @param machine number or Line/Order number.
	 */
	public SearchDocumentPageObjects searchAndEditDocument(String document) {
		List<String> records = new ArrayList<String>();
		int row_num=1;
		  for (WebElement tdElement : tdMachineNumber ){
			  records.add(tdElement.getText());
			  row_num++;
		  }
		  String temp = String.valueOf(records.indexOf(document)+1);
		  getWebDriver().findElement(By.xpath(".//*[@id='SearchDocumentTBody']/tr["+temp+"]/td[6]/.//*[@id=\"editbtn\"]")).click();
		return this;
	}
	
	
	public boolean isMachineNumberPresent(String machineNumber) {
		for (WebElement tdElement : tdMachineNumber ){
			 if(tdElement.getText().contains(machineNumber))
				return true;
		}
		return false;
	}
	
	public boolean isOrderNumberPresent(String orderNumber) {
		for (WebElement tdElement : tdOrderNumber ){
			 if(tdElement.getText().contains(orderNumber))
				return true;
		}
		return false;
	}
		
	public SearchDocumentPageObjects verifyMachineNumberIsNotPresent(String machineNumber) {
		Assert.assertFalse(isMachineNumberPresent(machineNumber), "Oops!!! Cannot find machine number");
		return this;
	}
	
	public SearchDocumentPageObjects verifyMachineNumberIsPresent(String machineNumber) {
		Assert.assertTrue(isMachineNumberPresent(machineNumber), "Oops!!! Cannot find machine number");
		return this;
	}
	
	public SearchDocumentPageObjects verifyOrderNumberFromGrid(String orderNumber) {
		Assert.assertFalse(isMachineNumberPresent(orderNumber), "Oops!!! cannot find ordernumber");
		return this;
	}
	
	public SearchDocumentPageObjects openSamplePDF() throws Exception {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, testPDFLink);
		click(testPDFLink);	
		return this;		
	}
	public SearchDocumentPageObjects downloadFile() throws Exception {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, btnDownloadRecord);
		click(btnDownloadRecord);	
		return this;		
	}
	
	public SearchDocumentPageObjects verifyFileDownloadErrorMessage() {
		Assert.assertTrue(msgFileDownload.getText().contains("Please select any one checkbox."));
		return this;
	}
	
	public SearchDocumentPageObjects verifyNumberOfRecordsInDatatable(int pageSize) {
		Assert.assertEquals(getNumberOfRecords(), pageSize,"Record size doenot match.");
		return this;
	}
	
	public int getNumberOfRecords() {
		int position = 0;
		List<String> records = new ArrayList();
		for (WebElement tdElement : tdSerialNumber) {
			records.add(tdElement.getText());
		}
		return records.size();
	}
	
	String getNumberOfRecordsInString() {
		return Integer.toString(tdSerialNumber.size());
	}
	
	public SearchDocumentPageObjects clearTxtSearchByOrderNumber() {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, txtOrderNumber);
		txtOrderNumber.clear();
		return this;
	}
	
	public SearchDocumentPageObjects verifyIfPaginationExsists() {
		boolean b = false;
		System.out.println("Pagination size:" + pagination.size());
		if(pagination.size() >=1)
			b = true;
		Assert.assertTrue(b);
		return this;
	}
	
	//Showing 1 to 10 of 21 entries
	
	List getPages() {
		List<String> records = new ArrayList();
		for (WebElement tdElement : pagination ){
			 records.add(tdElement.getText());
		}
		List<String> defaultRecords = Arrays.asList(paginationEntries);
		return new ArrayList<>(CollectionUtils.subtract(records, defaultRecords));
	}
	
	public SearchDocumentPageObjects verifyPaginationMessageDisplaysCorrectNumberOfRecords() {
		String paginationMessage = paginationDetails.getText();
		String records = paginationMessage.substring(paginationMessage.indexOf("of") + 3, paginationMessage.length());
		System.out.println(records);
		for (WebElement element : pagination) {
			System.out.println(element.getText());
		}
		Assert.assertTrue(records.contains(getNumberOfRecordsInString() + " entries"));
		return this;
	}
	
	public SearchDocumentPageObjects paginationSolution(int numberOfRecords, int entriesPerPage) {
		int totalPages = (numberOfRecords / entriesPerPage) + 1; // default first page.->
		int recordsOnLastPage = numberOfRecords % entriesPerPage; // 1 record on last page. 21%10 = 1
		List<Integer> actualRecordsPerPage = new ArrayList();
		for (int j = 0; j < pagination.size(); j++) {
			for (int i = 0; i <= getPages().size(); i++) {
				if (pagination.get(j).getText().contains(Integer.toString(i))) {
					click(pagination.get(j));
					actualRecordsPerPage.add(getSerialNumbers().size());
				}
			}
		}

		System.out.println("Actual records: " + actualRecordsPerPage);
		return this;
	}
	
	public SearchDocumentPageObjects verifyPaginationForAllRecords() {
		 return this;
	}
	
	public List<String> getSerialNumbers() {
		List<String> serialNumbers = new ArrayList<String>();
		for (WebElement element : tdSerialNumber) {
			serialNumbers.add(element.getText());
		}
		return serialNumbers;
	}
	
	public SearchDocumentPageObjects verifyUserIsOnSearchDocumentPage() {
		Assert.assertTrue(btnSearch.isDisplayed()); 
		return this;
	}
	
	public SearchDocumentPageObjects checkAllCheckboxes() {
		for(int i=1;i<=10;i++) {
			WebElement checkBox = getWebDriver().findElement(By.xpath(".//*[@id='SearchDocumentTBody']/tr["+i+"]/td[2]/.//*[contains(@id, 'CB')]"));
			FluentWaiting.waitUntillElementToBeClickable(5, 500, checkBox);
			click(checkBox);
		}
		sleep(1000);
		return this;	
	}
	
	public SearchDocumentPageObjects downloadFile(String fileName) {
		List<String> records = new ArrayList<String>();
		for (WebElement tdElement : tdMachineNumber ){
			  records.add(tdElement.getText());
		}
		String temp = String.valueOf(records.indexOf(fileName)+1);
		scrollToVisibleElement( getWebDriver().findElement(By.partialLinkText(fileName + ".pdf")));
		getWebDriver().findElement(By.xpath(".//*[@id='SearchDocumentTBody']/tr["+temp+"]/td[6]/.//*[@id=\"Viewbtn\"]")).click();
		return this;
	}
	
	public SearchDocumentPageObjects clickPaginationLast() {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, paginationLast);
		click(paginationLast);	
		System.out.println("Pagination last class: "+paginationLast.getAttribute("class"));
		return this;
	}
	
	public SearchDocumentPageObjects clickPaginationNext() {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, paginationNext);
		click(paginationNext);	
		System.out.println("Pagination last class: "+ paginationNext.getAttribute("class"));
		return this;
	}
	
	public SearchDocumentPageObjects clickPaginationPrevious() {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, paginationPrevious);
		click(paginationPrevious);	
		System.out.println("Pagination last class: "+ paginationPrevious.getAttribute("class"));
		return this;
	}
	
	public SearchDocumentPageObjects scrollToVisibleElement(WebElement element) {
		((JavascriptExecutor) getWebDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
		sleep(5000);
		return this;
	}
	
	public SearchDocumentPageObjects downloadMultipleFiles() {
		List files = getFileNames();
		for(int i=0;i<files.size();i++) {
			String temp = String.valueOf(files.indexOf(files.get(i))+1);
			  scrollToVisibleElement( getWebDriver().findElement(By.partialLinkText(files.get(i) + ".pdf")));
			  getWebDriver().findElement(By.xpath(".//*[@id='SearchDocumentTBody']/tr["+temp+"]/td[6]/.//*[@id=\"Viewbtn\"]")).click();
		}
		return this;
	}
	
	public List<String> getFileNames(){
		List<String> records = new ArrayList<String>();
		for (WebElement tdElement : tdMachineNumber ){
			  records.add(tdElement.getText());
		}
		return records;
	}
	
	public SearchDocumentPageObjects selectFileToDownload(String fileName) {
		List<String> records = new ArrayList<String>();
		for (WebElement tdElement : tdMachineNumber ){
			  records.add(tdElement.getText());
		}
		  System.out.println("Records" + records);
		  String temp = String.valueOf(records.indexOf(fileName)+1);
		  scrollToVisibleElement( getWebDriver().findElement(By.partialLinkText(fileName + ".pdf")));
		  getWebDriver().findElement(By.xpath(".//*[@id='SearchDocumentTBody']/tr["+temp+"]/td[2]/.//*[contains(@id, 'CB')]")).click();
		return this;
	}
	
	public SearchDocumentPageObjects isLoaderDisplayed() {
		FluentWaiting.waitUntillElementToBeVisible(5, 500, spinnerLoader);
		System.out.println("Spinner is displayed in between search operation.");
		return this;
	}
	
	public SearchDocumentPageObjects verifyEmptySearchMessage() {
		System.out.println(msgEmptySearch.getText());
		Assert.assertTrue(msgEmptySearch.getText().contains("S.O./Line Number or Machine Number is required."),"Error Message is wrong or Message is not displayed.");
		return this;
	}
	
	public SearchDocumentPageObjects listPdfs() {
		for (WebElement tdElement : pdfLinks){
			System.out.println(tdElement.getText());
		}
		return this;
	}
	
	public SearchDocumentPageObjects openPdf(String pdfText) {
		WebElement link = getWebDriver().findElement(By.partialLinkText( pdfText + ".pdf"));
		FluentWaiting.waitUntillElementToBeClickable(5, 500, link);
		click(link);	
		return this;
	}
	
	public SearchDocumentPageObjects searchFromGrid(String searchString) throws Exception {
		tableSearch.sendKeys(searchString);
		hitEnter();
		return this;
	}
	
	public SearchDocumentPageObjects checkAllRecordsFromGridMaually() throws Exception {
		int totalNumberOfRecords = Integer.parseInt(getNumberOfRecordsInString());
		for(int i=1;i<=totalNumberOfRecords;i++) {
			WebElement checkBox = getWebDriver().findElement(By.xpath(".//*[@id='SearchDocumentTBody']/tr["+i+"]/td[2]/.//*[contains(@id, 'CB')]"));
			FluentWaiting.waitUntillElementToBeClickable(5, 500, checkBox);
			click(checkBox);
		}
		sleep(1000);
		return this;
	}
	
	public SearchDocumentPageObjects selectSequentilRecordsFromPage(int numberOfRecords) throws Exception {
		for(int i=1;i<=numberOfRecords;i++) {
			WebElement checkBox = getWebDriver().findElement(By.xpath(".//*[@id='SearchDocumentTBody']/tr["+i+"]/td[2]/.//*[contains(@id, 'CB')]"));
			FluentWaiting.waitUntillElementToBeClickable(5, 500, checkBox);
			click(checkBox);
		}
		sleep(1000);
		return this;
	}
	
	public SearchDocumentPageObjects verifyIfSelectedRecordsInGridAndFileSystemAreEqual(List<String> gridRecords,List<String> fileSystemRecords) throws Exception {
		String oldString  = "";
		for (int i = 0; i < fileSystemRecords.size(); i++) {
			if (fileSystemRecords.get(i).contains(".")) {
				oldString = fileSystemRecords.get(i).substring(0, fileSystemRecords.get(i).lastIndexOf("."));
				fileSystemRecords.set(i, oldString);
	        }
		}
		Collections.sort(gridRecords);
		Collections.sort(fileSystemRecords);
		Assert.assertEquals(gridRecords, fileSystemRecords,"Records doesnot match");
		return this;
	}
	
	
}
