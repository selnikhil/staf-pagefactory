package pageObjects.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import pageObjects.initializePageObjects.PageFactoryInitializer;
import utils.FluentWaiting;

public class NavigationPageObjects extends PageFactoryInitializer {
	
	@FindBy(xpath="")
	private WebElement linkHome;
	
	@FindBy(xpath="//a[text()='Search Documents']")
	private WebElement linkSearchDocument;
	
	@FindBy(xpath="//a[text()='Upload Documents']")
	private WebElement linkUploadDocument;
	
	@FindBy(id="usernamelogin")
	private WebElement linkUserName;
	
	@FindBy(xpath="//*[@id=\"bs-example-navbar-collapse-1\"]/ul/li[4]/ul/li[1]/a/span")
	private WebElement linkChangePassword;
	
	@FindBy(xpath="//*[@id=\"bs-example-navbar-collapse-1\"]/ul/li[4]/ul/li[2]/a/span")
	private WebElement linkLogout;
	
	public NavigationPageObjects gotoHomePage() {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, linkSearchDocument);
		click(linkHome);
		return this;
	}
	
	public NavigationPageObjects gotoSearchDocumentsPage() {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, linkSearchDocument);
		click(linkSearchDocument);
		return this;
	}
	
	public NavigationPageObjects gotoUploadDocumentsPage() {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, linkUploadDocument);
		click(linkUploadDocument);
		return this;
	}
	
	public NavigationPageObjects clickOnUsername() {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, linkUserName);
		click(linkUserName);
		return this;
	}
	
	public NavigationPageObjects gotoChangePasswordPage() {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, linkChangePassword);
		click(linkChangePassword);
		return this;
	}
	
	public NavigationPageObjects logoutApplication() {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, linkLogout);
		click(linkLogout);
		return this;
	}
	
	public NavigationPageObjects signOut() {
		click(linkUserName);
		FluentWaiting.waitUntillElementToBeClickable(5, 500, linkLogout);
		click(linkLogout);
		return this;
	}
	
	public NavigationPageObjects verifyLoggedInUser(String username) {
		Assert.assertTrue(linkUserName.getText().contains(username));
		return this;
	}
}
