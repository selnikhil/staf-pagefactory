/**
 * 
 */
package pageObjects.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import pageObjects.initializePageObjects.PageFactoryInitializer;


/**
 * @Author Nikhil Bhole
 * @Date 04-July-2018
 */
public class GMailPageObjects extends PageFactoryInitializer
{
	@FindBy(xpath="//input[@type='email']")
	private WebElement emailIDTextBox;
	
	@FindBy(xpath="//span[contains(text(),'Next')]")
	private WebElement nextButton;

	
	public GMailPageObjects enterEmailID(String emailID) 
	{
		utils.FluentWaiting.waitUntillElementToBeClickable(30, 500, emailIDTextBox);
		emailIDTextBox.sendKeys(emailID);	
		nextButton.click();
		return this;
	}

	
	public GMailPageObjects verifyPageTitle() throws Exception 
	{
		Assert.assertEquals(getWebDriver().getTitle(), "gagagasgasg");
		return this;
	}
	
	
	
}
