package pageObjects.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import pageObjects.initializePageObjects.PageFactoryInitializer;
import utils.FluentWaiting;

public class PopupModal extends PageFactoryInitializer {
	
	
	@FindBy(xpath = "//button[contains(.,'OK')]")
	private WebElement btnOk;
	
	@FindBy(xpath="//button[contains(.,'Yes')]")
	private WebElement btnYes;
	
	@FindBy(xpath="//button[contains(.,'No')]")
	private WebElement btnNo;
	
	@FindBy(xpath="")
	private WebElement btnDelete;
	
	@FindBy(xpath = "//button[contains(.,'Cancel')]")
	private WebElement btnCancel;
	
	@FindBy(xpath = ".//*[@id='swal2-content']")
	private WebElement response;
	
	//swal2-content
	
	@FindBy(xpath = ".//*[@id='swal2-content']")
	private WebElement swalTwoContent;
	
	@FindBy(xpath = "//h4[@class='modal-title']")
	private WebElement modalTitle;
	
	public PopupModal clickYes() {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, btnYes);
		click(btnYes);
		return this;
	}
	
	public PopupModal clickNo() {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, btnNo);
		click(btnNo);
		return this;
	}
	
	public PopupModal clickOk() {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, btnOk);
		click(btnOk);
		return this;
	}
	
	public PopupModal clickCancel() {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, btnCancel);
		click(btnCancel);
		return this;
	}
	
	public PopupModal verifyMessageAfterDeletingRecord() {
		Assert.assertTrue(response.getText().contains("Document delete successfully."),"Error Message doesnot matches. Error message maybe wrong.");
		return this;
	}
	
	public PopupModal verifyConfirmDeleteMessage() {
		Assert.assertTrue(response.getText().contains("Are you sure to delete this document?"),"Confirm delete message is displayed wrong.");
		return this;
	}
	
	public String getMessageFromModal() {
		return response.getText();
	}
	
	public String getModalTitle() {
		return modalTitle.getText();
	}
}
