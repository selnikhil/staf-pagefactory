package pageObjects.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import pageObjects.initializePageObjects.PageFactoryInitializer;
import utils.FluentWaiting;

public class ChangePasswordPageObjects extends PageFactoryInitializer{
	
	/* PAGE OBJECTS */
	
	@FindBy(id="CurrentPassword")
	private WebElement txtOldPassword;
	
	@FindBy(xpath="//*[@id=\"Currentpassworderrormessage\"]")
	private WebElement msgOldPassword;
	
	@FindBy(id="NewPassword")
	private WebElement txtNewPassword;
	
	@FindBy(xpath = "//*[@id=\"NewPassworderrormessage\"]")
	private WebElement msgNewPassword;
	
	@FindBy(id="ConfirmPassword")
	private WebElement txtConfirmPassword;
	
	@FindBy(xpath = "//*[@id=\"ConfirmPassworderrormessage\"]")
	private WebElement msgConfirmPassword;
	
	@FindBy(id="ChangePasswordButton")
	private WebElement btnChangePassword;
	
	@FindBy(xpath = "//*[@id=\"body\"]/fieldset[2]/p")
	private WebElement msgNewAndConfirmPasswordMatch;
	
	@FindBy(xpath = "//*[@id=\"body\"]/fieldset[1]/p")
	private WebElement msgIncorrectCurrentPassword;
	
	
	/* APPLICATION MODULES */
	
	public ChangePasswordPageObjects enterOldPassword(String oldPassword) {
		txtOldPassword.sendKeys(oldPassword);
		return this;
	}
	
	public ChangePasswordPageObjects enterNewPassword(String newPassword) {
		txtNewPassword.sendKeys(newPassword);
		return this;
	}
	
	public ChangePasswordPageObjects enterConfirmPassword(String confirmPassword) {
		txtConfirmPassword.sendKeys(confirmPassword);
		return this;
	}
	
	public ChangePasswordPageObjects clickChangePassword() {
		FluentWaiting.waitUntillElementToBeClickable(5, 500, btnChangePassword);
		click(btnChangePassword);
		return this;
	}
	
	public ChangePasswordPageObjects changePassword(String oldPassword, String newPassword, String confirmPassword) {
		enterOldPassword(oldPassword);
		enterNewPassword(newPassword);
		enterConfirmPassword(confirmPassword);
		clickChangePassword();
		return this;
	}
	
	public ChangePasswordPageObjects verifyEmptyMessageOnEmptyOldPassword() {
		Assert.assertTrue(msgOldPassword.getText().equals("Current Password required."),"Empty old password message doesnot maches");
		return this;
	}
	
	public ChangePasswordPageObjects verifyEmptyMessageOnEmptyNewPassword() {
		Assert.assertTrue(msgNewPassword.getText().equals("New Password required."),"Empty new password message doesnot maches");
		return this;
	}
	
	public ChangePasswordPageObjects verifyEmptyMessageOnEmptyConfirmPassword() {
		Assert.assertTrue(msgConfirmPassword.getText().equals("Confirm Password required."),"Empty confirm password message doesnot maches");
		return this;
	}
	
	public ChangePasswordPageObjects verifyNewPasswordAndConfirmPasswordMatches() {
		Assert.assertTrue(msgNewAndConfirmPasswordMatch.getText().equals("Password and confirm password is not matched"),"Error message is not displayed if "
				+ "confirm password and new password are not the same.");
		return this;
	}
	
	public ChangePasswordPageObjects verifyNewPasswordLength() {
		Assert.assertTrue(msgNewAndConfirmPasswordMatch.getText().equals("New password must be minimum 6 characters."),"Error message is not displayed if "
				+ "confirm password and new password are not the same.");
		return this;
	}
	
	public ChangePasswordPageObjects verifyIncorrectCurrentPassword() {
		Assert.assertTrue(msgIncorrectCurrentPassword.getText().equals("Incorrect current password."),"Incorrect Password message is not displayed when incorrect curre"
				+ "nt password is entered");
		return this;
	}
	
	public ChangePasswordPageObjects verifySuccessMessageOnPasswordChange() {
		Assert.assertTrue(modal().getMessageFromModal().contains("Password Changed Successfully"));
		return this;
	}
}
