package utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * The ReadConfigurationProperty class is used to read the configuration details from
 * ApplicationConfig file
 *
 * @author Nikhil_B
 * @version 1.0
 * @since May 2018
 * 
 */

public class ApplicationConfigReader {
	
	static InputStream input = null;
	static String filename = "ApplicationConfig.properties";
	static Properties properties = new Properties();
	
	public static String getConfigValues(String elementValue) {

		try {
			input = ApplicationConfigReader.class.getClassLoader().getResourceAsStream(filename);
			if (input == null) {
				return "Sorry, unable to find " + filename;
			}
			properties.load(input);
		} catch (IOException ex) {

			ex.printStackTrace();

		} finally {

			if (input != null) {
				try {

					input.close();

				} catch (IOException e) {

					e.printStackTrace();
				}
			}

		}
		return properties.getProperty(elementValue);
	}
}
