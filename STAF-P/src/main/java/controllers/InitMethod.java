package controllers;

import java.awt.Robot;
import java.io.File;
import java.net.URI;

import org.openqa.selenium.Alert;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.ITestResult;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import utils.ApplicationConfigReader;

/**
 * @Author Nikhil Bhole
 * @Date 04-July-2018

 */

public class InitMethod {
	
	public static String WebsiteURL = ApplicationConfigReader.getConfigValues("Url");
	public static String Browser = ApplicationConfigReader.getConfigValues("Browser");
	public static int MaxPageLoadTime = Integer.parseInt(ApplicationConfigReader.getConfigValues("MaxPageLoadTime"));
	public static int ImplicitlyWait = Integer.parseInt(ApplicationConfigReader.getConfigValues("ImplicitlyWait"));
	public static String VideoFeature = ApplicationConfigReader.getConfigValues("VideoFeature");
	
	public static String FS = File.separator;

	public static String OSName = System.getProperty("os.name");
	public static String OSArchitecture = System.getProperty("os.arch");
	public static String OSVersion = System.getProperty("os.version");
	public static String OSBit = System.getProperty("sun.arch.data.model");

	public static String ProjectWorkingDirectory = System.getProperty("user.dir");
	
	public static String ExcelFiles = "./src/test/resources/Excel Files/";
	public static String TestData = "./src/test/resources/TestData/";
	public static String PropertiesFiles = "./src/test/resources/Properties Files";
	public static String Reports = "./src/test/resources/Reports";
	
	public static Robot re;
	public static Alert al;
	public static String robotImageName;
	public static Select se;
	public static String FileToUpload;
	public static Actions ac;
	public static ITestResult testResult;
	public static SoftAssert softAssert;
	public static ITestResult result;
	public static URI uri;
	public static ExtentReports extent;
	public static ExtentTest test;
	
}
