/**
 * 
 */
package controllers;

import java.io.File;
import java.lang.reflect.Method;

import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.relevantcodes.extentreports.ExtentReports;

import io.github.bonigarcia.wdm.EdgeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import io.github.bonigarcia.wdm.InternetExplorerDriverManager;
import io.github.bonigarcia.wdm.PhantomJsDriverManager;

/**
 * @Author Nikhil Bhole
 * @Date 04-July-2018
 */


public class WebDriverFactory extends BrowserFactory
{
	public static ThreadLocal<WebDriver> wd = new ThreadLocal<WebDriver>();
	public static String browser;
	public static String url;

	@BeforeSuite
	public void extentSetup() 
	{
		extent = new ExtentReports("/home/nikhil/selenium-automation/staf-pagefactory/STAF-P/TestReports/AutomationReport.html");
		extent.loadConfig(new File("/home/nikhil/selenium-automation/staf-pagefactory/STAF-P/extent-config.xml"));
	}
	
	@BeforeTest(alwaysRun=true)
	public void suiteSetup() throws Exception
	{
		switch(Browser.toLowerCase())
		{
		case "chrome":
		case "chrome_headless":
		case "opera":
			System.setProperty("webdriver.chrome.driver", "/home/nikhil/Documents/Webdrivers/Chromedriver/chromedriver");
			break;

		case "firefox":
			FirefoxDriverManager.getInstance().setup();
			break;

		case "ie":
		case "internet explorer":
			InternetExplorerDriverManager.getInstance().setup();
			break;	

		case "edge":
			EdgeDriverManager.getInstance().setup();
			break;

		case "ghost":
		case "phantom":
			PhantomJsDriverManager.getInstance().setup();
			break;
			
		case "safari":
			break;

		default:
			throw new NotFoundException("Browser Not Found. Please Provide a Valid Browser");
		}
	}

	@BeforeMethod
	public void beforeMethod(Method method) throws Exception
	{
		test = extent.startTest((this.getClass().getSimpleName() + " :: " + method.getName()));
		test.assignAuthor("Nikhil Bhole");
		test.assignCategory("smoke-");
		System.out.println("Browser: "+Browser);
		System.out.println("WebsiteURL: "+WebsiteURL);
		new WebDriverFactory();
		WebDriver driver = WebDriverFactory.createDriver();
		setWebDriver(driver);
		
	}
	
	public void setWebDriver(WebDriver driver) 
	{
		wd.set(driver);
	}

	public static WebDriver getWebDriver() 
	{
		return wd.get();
	}

	@AfterMethod(alwaysRun=true,enabled=true)
	public void afterMethod() throws Exception
	{
		Thread.sleep(2000);
		getWebDriver().quit();	
	}
	
	@AfterSuite
	public void afterSuite() {
		extent.flush();
	}
}
